{
    "id": "4aa44049-5dab-4ff5-b85d-17f1769959d2",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt1",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "SmallBugPixel",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0fc60b4b-494e-4b9e-8ff2-7b356b185d82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "1db64069-bd0d-4f7b-8700-403bb99d219e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 100,
                "y": 34
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d8c9c478-e84b-490c-8229-1eedd26630b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 95,
                "y": 34
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2b535dfd-a681-4b0b-94ae-a1b74a101014",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 88,
                "y": 34
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4a78406a-c50c-470d-ad8b-af3f03a208ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 83,
                "y": 34
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "97df3645-0819-4c40-b11f-6ff535905802",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 76,
                "y": 34
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "36ee50e9-9fcb-427a-a387-25aa544f47a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 69,
                "y": 34
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "dad8c317-d470-4d6f-ad04-9287e04640d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 66,
                "y": 34
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "30aa72ca-fb81-484e-84f4-f92612fd9ee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 62,
                "y": 34
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1fd45297-ed18-4d0f-8cdf-9fafaf545f45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 58,
                "y": 34
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b066a45a-8028-492c-9f32-c7fd16d9742a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 103,
                "y": 34
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "00a336db-2489-42b8-b780-466e50f3ba1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 53,
                "y": 34
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "dfa4d301-2b78-41f5-9520-c315391824f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 45,
                "y": 34
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "735e0fac-ba9f-4d60-9f8d-3a7d5396ef8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 40,
                "y": 34
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ef65133b-2e87-4848-81e7-22f95eb1d23b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 37,
                "y": 34
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "81397aff-8449-4ad7-a4e4-7b9c3fd96874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 32,
                "y": 34
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9582bfde-81e5-4a08-a832-967911b038ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 27,
                "y": 34
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5bebe758-7da2-4567-a400-48a0e3ec4726",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 23,
                "y": 34
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "86ac3257-dacc-42f0-a2f0-8c8b0e5a3366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 18,
                "y": 34
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8ba1a661-0113-49a6-8491-2af8fa89f312",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 13,
                "y": 34
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e42f69cc-4542-483b-a1b3-12992a373501",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 8,
                "y": 34
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "09c2d7b1-81d0-44a6-8e84-240a57ea920b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 48,
                "y": 34
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "18aba741-5189-46fa-9e1f-789c52e350c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 108,
                "y": 34
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f275350a-5e4f-40aa-bbdb-1975ce5d86e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 113,
                "y": 34
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6e3cda39-1564-47cc-920e-ced0c1842e55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 118,
                "y": 34
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ef8f8b81-dd04-4fbf-8fb5-4493c4c2d23b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 111,
                "y": 50
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "da7837ac-9c89-4ee6-8e69-31f7eb59f07f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 108,
                "y": 50
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "586cea41-3da8-4c99-aec8-0fe06d103d11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 105,
                "y": 50
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "682ea2c9-5a88-4066-ab96-9916bdde5c38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 100,
                "y": 50
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b4deea9a-bb8b-4ef9-a66c-987f4a34e8d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 95,
                "y": 50
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "dda6800e-174a-4d81-ac0d-eb93193181ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 90,
                "y": 50
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a46ccd0d-e28d-4d83-89b2-886334cecbb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 85,
                "y": 50
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e28fa01b-6ff8-445f-9aa5-24afca7f6b08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 76,
                "y": 50
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f4aa333f-1185-4359-be65-6a1f7499ef98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 70,
                "y": 50
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "125b77f7-07f1-4667-abb7-f63ad1cc3463",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 64,
                "y": 50
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5fb9e583-9e9f-4168-b346-900d34833747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 58,
                "y": 50
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "059409c3-a2de-4aa6-a8fd-63f513d82892",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 52,
                "y": 50
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "da61faa0-03bb-49f9-969d-20cdd9072c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 47,
                "y": 50
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "16b3ae58-fb9f-4a5c-aab7-4fe99b0601d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 42,
                "y": 50
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4d6a3d17-998b-4315-b8de-5e3f9be5fb1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 36,
                "y": 50
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e4d8be5f-77f4-4e76-b0c3-36e75e66f03f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 30,
                "y": 50
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "06b7f628-2496-4c78-9a6a-ac55fc81c239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 25,
                "y": 50
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ad6a7c1f-a284-4641-93f8-b902919fa7dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 20,
                "y": 50
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e672264f-074b-436d-92d3-ebcf9339cc99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 14,
                "y": 50
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2b69edda-ee66-4e4b-8654-ada4892a7d1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 9,
                "y": 50
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e6c604e7-9434-4b43-95e6-fb7dff8d5c0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "aee768f8-d644-4bf0-aaa0-c91dbbcc6335",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f3175a0f-7b22-427a-b559-673c8b4c118d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 118,
                "y": 18
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "dbb69db5-b851-4f8f-93b7-f7708fb11193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 112,
                "y": 18
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a234e11a-3717-4441-8663-99d4d863c968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ccd9aac5-f700-44dc-a0c4-ef0752a2173d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "dbffed6e-9de1-4b36-8ed9-aac1b1b304a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "aa4bf2de-e0a6-4bd7-8619-330fd8c497ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "5543c0cc-33a2-43fe-855f-bed20f30dfe4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f5a10d2a-8a22-4003-a290-04011955fbb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "202240a0-001a-40c4-937c-7064a90fcb12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1a17f816-19f1-4115-a425-3c5c150195dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "27e18363-11cd-468c-87ea-5cecbd75cbc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d0ea0fb3-a700-4e26-b6aa-763bcc51c835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9ae3617a-22dd-4140-8505-9ead6712caa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b98a3e46-2c38-4ebb-8468-1bfe2c49a086",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "abfc0150-720f-47d8-91ac-c40f91c2a717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "dccdf70a-9e76-4e9a-a3d5-343e1870d0e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "933ac459-38b0-470a-89ea-e635eae892d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "43b9a306-ecc0-4d03-9a20-414bd4efbd69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "96b20b07-5747-466a-bd18-35ed784a3f33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "43c4a3a1-ba7f-4ab3-8685-69c5e9d0e8ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "291d7b1b-272e-482c-b1d5-2c4fff517683",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "42ace588-440c-484d-94e3-2e716dfffd65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "67c3e957-10b9-44c6-9f7a-b4648e7c5951",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1dfc8ac4-70e3-4135-8406-c06fc074a20f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "49a23cfa-8f32-4157-8160-fd80c6e00492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 2,
                "y": 18
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "76d8c498-d5a3-4e64-a015-6d3afe1b15bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 55,
                "y": 18
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "527aa47d-6752-4790-8381-5f315fe67288",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 7,
                "y": 18
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "2c167dc6-89e4-44e1-a3c6-8ff0e98b4c22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 103,
                "y": 18
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "11569911-95b5-43bc-b480-36b58ebbfecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 98,
                "y": 18
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f686313e-077c-42a8-8a0c-3f40843bd265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 95,
                "y": 18
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "969d5bc1-672f-4ccd-b3f4-9edd38a35d0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 88,
                "y": 18
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "125dc8bb-e6c5-4cfe-ab8e-2728ef31cc65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 83,
                "y": 18
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "92f47864-5456-492d-aecf-2207fce362b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 78,
                "y": 18
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "13a55639-0805-4363-a45c-4382d0565a5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 73,
                "y": 18
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c6a6edac-b783-465b-bb71-5aba75d29ae3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 68,
                "y": 18
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e8d87420-92ff-4dc1-8041-9b3c2e1832be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 64,
                "y": 18
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4883830a-a703-484d-bf35-0f42b6c0befe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 107,
                "y": 18
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b6911d03-489a-41ef-92be-b8a9ce498c0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 60,
                "y": 18
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "32bd3c02-2ac6-4eb7-b9db-03a76a5c807a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 50,
                "y": 18
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ae2fd296-c817-4de4-a431-1a32bebf8155",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 45,
                "y": 18
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "418d3c21-bd2d-489a-96f5-e7f324e8d615",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 38,
                "y": 18
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a1eb1009-e3b8-4b42-a28c-527f51ea111b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 33,
                "y": 18
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "66606e66-87bd-42b9-8f7a-056ac668cc09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 28,
                "y": 18
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9cfc5ea6-cc04-4193-aaaf-bb017ca519bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 23,
                "y": 18
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c62a479f-0a59-4ca4-9f88-02314bc6a285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 18,
                "y": 18
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "12f81717-e3e3-413b-b32a-61a0318a4999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 15,
                "y": 18
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5e083927-83ef-4d19-9822-23bf0d83bdc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 10,
                "y": 18
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7fcdef08-9ad6-4853-a33d-d02dae072ac7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 116,
                "y": 50
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "5f9f8c52-bef6-4a88-9a6f-55e6c06ed361",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 14,
                "offset": 3,
                "shift": 16,
                "w": 9,
                "x": 2,
                "y": 66
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}