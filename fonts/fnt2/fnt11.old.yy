{
    "id": "4aa44049-5dab-4ff5-b85d-17f1769959d2",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt1",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "SmallBugMonospac",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "5f40d274-f4c6-435c-8069-fa5e78b777fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8fa42572-4dd3-4c75-98c5-00e14f8f74b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 3,
                "shift": 9,
                "w": 1,
                "x": 53,
                "y": 70
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f7a3fc90-3947-4ade-a932-1e80553e043a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 47,
                "y": 70
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3c988bf0-a850-4625-8cbf-ee92bd5df2d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 37,
                "y": 70
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "09d25353-d482-43f8-ae41-07587cb1ba70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 28,
                "y": 70
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "61d080ed-86f0-4848-9315-82cdd7e2d81a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 19,
                "y": 70
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "0ad1a677-13c4-46e0-81e3-43668863cb99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 11,
                "y": 70
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "838169ba-68d9-43d4-90d2-9b949df6b3d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 4,
                "shift": 9,
                "w": 1,
                "x": 8,
                "y": 70
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "aa946fa2-f840-4939-8caf-2f6ef593b80e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0eeb7d8c-12df-4cd5-9e03-43b7dcf89ff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 117,
                "y": 53
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d16b5432-6986-4d06-b7ff-cfd4955356f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 56,
                "y": 70
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "85d56d1c-1025-463c-8e43-8085aa9a985e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 108,
                "y": 53
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ec3fe47c-4652-445c-b40c-d782d7422c88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 2,
                "x": 96,
                "y": 53
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "858da89d-91f7-4bc7-8ed8-c1a01989c2bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 89,
                "y": 53
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5e4bdfef-b19a-41d4-91f1-45b33a94e1bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 2,
                "x": 85,
                "y": 53
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9ec5e26c-5c4c-4f85-bb97-ed818691b942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 78,
                "y": 53
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0a7677c8-4d30-432b-8ccb-9687365dc779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 70,
                "y": 53
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b6e3ab1c-65d2-4dce-b845-c7fe7f131cbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 63,
                "y": 53
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "491f6f24-1bb7-41d2-90ae-38691f2a2f50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 55,
                "y": 53
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4e484fec-1fcf-4a28-8783-d210d1c2901d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 47,
                "y": 53
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7d937f12-a1e4-4a85-8a08-df37104cf247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 53
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9137a894-8f60-4399-aa64-380cfd670fd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 100,
                "y": 53
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "bda94b57-3909-4ad8-86de-9ccb2725d6ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 64,
                "y": 70
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2dea7a52-4a4f-4015-8d58-3c31840c14e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 72,
                "y": 70
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "443001e1-aa21-450d-af18-4e311e9a236b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 81,
                "y": 70
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c6087b50-1d64-4708-be78-d038b1811203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 6,
                "y": 104
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "9f3a7d54-3df8-4576-9f41-a1afbe6c4a3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 2,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "80a940b4-3480-4f44-8728-cee0994fba64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 2,
                "x": 121,
                "y": 87
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0b4c61be-221a-49f3-b8e2-c5fb6b78d6fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 113,
                "y": 87
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "eee5eec0-ba7f-42a5-b790-e929fecc3232",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 106,
                "y": 87
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5c046963-fb33-4dfb-9ad2-02036ecea256",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 98,
                "y": 87
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a866bdbb-bbef-49b9-bc17-79f47488c143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 91,
                "y": 87
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9dd9c122-5a98-40f8-b89f-65d5ef3acd65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 81,
                "y": 87
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6be4ad19-1a54-49a8-a411-192ee8b8a96c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 72,
                "y": 87
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "359656bd-7f73-4f72-a292-7b0d15bfdfa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 63,
                "y": 87
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8932df6a-c2dd-4cb0-be47-35ec24df1282",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 54,
                "y": 87
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ac603f7b-fa90-4056-a310-2904b346e24a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 45,
                "y": 87
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b107dc19-e4fb-4ae4-83a3-1f9f3d117654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 36,
                "y": 87
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e76d5d4a-16fc-4b32-b897-6f12efcbf361",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 27,
                "y": 87
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e5d576e3-53fb-495b-8d9b-335135d2e9a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 18,
                "y": 87
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "fbcfa977-fff6-455d-a4ea-13c8598a8290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 9,
                "y": 87
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1b321c4a-fd8f-4d2e-931e-8980ca955fe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 2,
                "y": 87
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "9048a17a-6aef-4dce-be47-4c7926e677a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 116,
                "y": 70
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "025544c0-8ed5-470b-a296-304624a7bf1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 107,
                "y": 70
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "5d65b69d-24e3-4d1f-af72-f7d63b945749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 98,
                "y": 70
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "3ec1964f-f366-43b5-83b1-5742fb17bea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 89,
                "y": 70
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c5ba558b-2354-4d0a-87f3-e3a89b304b23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 53
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9b766f88-07c3-4bea-99eb-1f6eb583d893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 53
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5e45570b-2393-4361-a968-f6481389be2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 53
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "28e1c838-4dbf-473e-8672-29038c365503",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 62,
                "y": 19
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7e31abe5-590a-43e6-a83e-78e90ca4b815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 47,
                "y": 19
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b622b151-142e-4b19-a648-5c5dc11edba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 19
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "dfb23aea-1ecd-41c2-8123-6490a4835dd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 19
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "263a3123-0636-46ca-be89-d18e3c7d152f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 19
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7c7b5f97-5337-4da8-aa35-69f475c7580e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 19
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "56034150-8e37-4c92-a249-72b6545871ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "cedd07a8-74e5-4b8e-a35e-974c4e09fdb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "37735008-aeb4-48f9-a6ac-95f13e5a925e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b6bea7c6-7a21-4161-8c1b-02e91f6ed8c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c2b7917c-fa75-45d8-be79-1b44bb10cca7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 56,
                "y": 19
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e87090f6-2c4d-4a35-8d95-c1746e2e845f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "0ec5cd43-b48d-4315-bcec-f70a3417ebea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "46de8281-260d-4a2e-81a3-f93e202684b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f2d9118b-a319-4f5a-82a1-86638e1c32f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1480d3e6-9ee3-481a-bd1f-7f11d1875c96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 3,
                "shift": 9,
                "w": 2,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "7e1626f4-460b-41d5-b8a5-b060da084033",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "cb0107a5-15e9-4409-90a0-6ccd2da70c5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "82a1ab91-81a8-4f63-8c58-a213b5714c2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "9b07b3e7-b322-4471-8f6d-661be97e84aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a450eb80-e97b-4de4-972b-92d22b3d7ba3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "f268949b-11c4-4dab-8d82-af80264e5b1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "cb5ca6cf-2472-4ec7-a1a8-e4b8573ac02d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 71,
                "y": 19
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "73367546-20e2-4559-8165-4063a4467987",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 36
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "46bb341e-75c6-438e-b135-36bcd0fa2e09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 80,
                "y": 19
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "895b51f0-7128-4857-a98c-13a033586848",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 4,
                "x": 115,
                "y": 36
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "82751ff6-30d0-4acc-b2c2-d90f26a3ee27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 107,
                "y": 36
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "567113f1-df25-4c91-ae0b-15c7d75e3380",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 100,
                "y": 36
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3fed712f-f765-4ba7-ba26-ec3e11ab33cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 91,
                "y": 36
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "08d099f6-f739-4283-9332-0379547a3f9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 82,
                "y": 36
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b75f6b9b-a00e-43c8-83e1-7ba69239cb3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 73,
                "y": 36
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6f41873f-d5f4-44dc-b5cb-a793db642700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 64,
                "y": 36
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "89bdc809-951a-4ac3-9b31-402cd0c1424e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 55,
                "y": 36
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2252013d-b0f5-4c4c-97b9-0ad0b0a3852a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 47,
                "y": 36
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "086cd26d-6c3d-426d-8f08-81b9ccadddb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "09443bc9-85ab-41c2-87ec-24c9b3042e55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 36
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "73f5321d-77dd-413a-a9cd-59508911ec79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 36
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "fb837bf2-d74a-4a38-99a4-d58c717feae3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 36
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8b73ac69-0c39-4a91-8776-428eeb48309d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4ae26cbf-2981-4c00-8401-3ba38b6dcf2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 118,
                "y": 19
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e363c866-5d74-44aa-992f-e39efea15245",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 109,
                "y": 19
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c76d9e07-ad94-4d86-b3b1-c763fcfa72f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 100,
                "y": 19
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "fc4e3e24-db24-4d04-b862-f9d97db96562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 95,
                "y": 19
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3da193dc-ddbd-4e2d-9b46-f0e4130def3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 4,
                "shift": 9,
                "w": 1,
                "x": 92,
                "y": 19
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5c06cc44-cd50-4e4b-9805-aa8f9c0429d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 87,
                "y": 19
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6ea30c47-85cd-4aa3-aed5-e6c5f6f14446",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 14,
                "y": 104
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4847bab6-ccd7-49cd-87da-f2cbdd615ef3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 15,
                "offset": 3,
                "shift": 16,
                "w": 9,
                "x": 22,
                "y": 104
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}