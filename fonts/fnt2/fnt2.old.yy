{
    "id": "6927cc0b-b3ec-434f-99af-b1e5d6486fef",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt2",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "SmallBugMonospac",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b7ecf456-345f-42c2-a852-0829d20aab4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d90b20c0-409c-4d09-86f0-6c182c1ecb3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 3,
                "shift": 9,
                "w": 1,
                "x": 53,
                "y": 70
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "406f9f99-219e-4aa6-a68f-0d9f04c5e03c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 47,
                "y": 70
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b16559af-3cd4-4b11-9278-dac2d56b8a00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 37,
                "y": 70
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4538111c-4248-4655-9dac-82261703564e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 28,
                "y": 70
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "1a31f09c-9348-4dba-aecc-cf3286b0e033",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 19,
                "y": 70
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "68f45bfc-7d87-4920-90e9-76973f26ea9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 11,
                "y": 70
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "df531420-d0c2-477e-ba9d-b4051c664fd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 4,
                "shift": 9,
                "w": 1,
                "x": 8,
                "y": 70
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4ebd838f-2991-4526-872d-a38ba156b85e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "dfafb150-8705-4581-bda5-d596a1584549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 117,
                "y": 53
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0d393ea0-2ecf-4005-93ae-023630522a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 56,
                "y": 70
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "425d4fdc-5bd5-4cbd-9bcf-eb1aae3ce010",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 108,
                "y": 53
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8c4c2422-21bd-47a8-8abb-63435f162140",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 2,
                "x": 96,
                "y": 53
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c54687fb-5c22-4678-be60-117b1cd6211d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 89,
                "y": 53
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7b0f7e2b-b31a-47dd-a290-75678f0e6e94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 2,
                "x": 85,
                "y": 53
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ac61ad85-2428-4513-a4bb-86da2ad917fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 78,
                "y": 53
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "dc9b17db-e705-4369-9ec0-a945d3bb3c7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 70,
                "y": 53
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f4baa5d9-a302-4174-ac3a-2e304792fd75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 63,
                "y": 53
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c03368a3-5926-4d7e-a832-6f8630ac88de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 55,
                "y": 53
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7c6133fe-506a-4fce-a1a2-92b3d676ad78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 47,
                "y": 53
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a66761a4-46b9-4fed-bbd8-1d5a6ee5ecee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 53
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f76715f1-fb3e-47d5-ad48-c4ccddef76a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 100,
                "y": 53
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "27b0a8a5-4953-4c3e-859f-cb1bbc4d4c8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 64,
                "y": 70
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0bd055a1-8430-4c35-bc3f-4cbc594f0910",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 72,
                "y": 70
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8772053f-b2f9-4efb-9adf-0d30f365d3d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 81,
                "y": 70
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c85dc2f9-9af7-4bb1-9f70-debc0ad655c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 6,
                "y": 104
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "4f1d2252-a0b4-4b18-be75-09bfa273cb55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 2,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "57ac72b2-ae0f-468d-ad18-86721ac0360d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 2,
                "x": 121,
                "y": 87
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ce44a049-2208-4eb8-ae36-53abf3235ea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 113,
                "y": 87
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7a971a0c-4009-4640-a53b-3e8bc36f0238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 106,
                "y": 87
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "43049642-34aa-46f3-98c4-a9df5fba96cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 98,
                "y": 87
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "53b46152-a6a2-4347-a562-42ec4a127a33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 91,
                "y": 87
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "888cf6a0-9a1f-471e-b737-b0c0fb43f8f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 81,
                "y": 87
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "598b9b3b-a9f6-4073-b3cb-b5a947beafb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 72,
                "y": 87
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "60f406d3-a5ea-46bb-a76a-e35154514c61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 63,
                "y": 87
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "150fba3e-5a11-4921-ad01-6dd205439a51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 54,
                "y": 87
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "8b7fec1c-9273-4806-b554-6cb634758e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 45,
                "y": 87
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "861a2ada-fa93-4871-844e-a430985925d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 36,
                "y": 87
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d44397d9-0c44-4eab-a2dc-f7c66e089232",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 27,
                "y": 87
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2cedbc80-ecf7-424b-a0e6-23f7cbd9a496",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 18,
                "y": 87
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4ab345e2-ad0e-4ae1-aabc-94c3f225c592",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 9,
                "y": 87
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5a600e31-2905-475c-8fbf-70dc40406aef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 2,
                "y": 87
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "9525a608-b483-4ffa-84db-e45c08cef15f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 116,
                "y": 70
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "826400c1-3a6b-483e-aa5a-0e397b086531",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 107,
                "y": 70
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f5c8121d-c2ae-49fa-a874-a74f78fcb41b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 98,
                "y": 70
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d147b47f-e67c-4e11-b1ca-c059c7bd75a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 89,
                "y": 70
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "22f0aa26-391c-45fe-8b39-3198091fccab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 53
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ff91ec01-c4cd-4cff-a8c8-03eac72c34bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 53
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f875e668-7d3a-47f3-b49f-3689be4fe8b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 53
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5e5204fd-5931-4b44-9bd5-6d2b921923e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 62,
                "y": 19
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "bbc81dd9-84d6-4aa8-a3e0-0c1a003cf509",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 47,
                "y": 19
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "53e776bf-7500-4e32-b396-8f6867cda9b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 19
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "bfa9eb00-7256-47ee-8587-fc7175bc2f5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 19
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "02634d1c-d44e-4323-8af1-ad8d72a02d27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 19
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "3f6600a1-b567-4588-a071-1ee23b2e722d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 19
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b5ef22a5-ee23-40d1-87a5-6a453993cfb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "db3c28b2-b896-4433-af67-bf0479773a84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5b57303d-6079-4f94-b1e0-ba6cfa8f8089",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b21406db-5ca6-4c7c-a568-f138bcfcd741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6acf1960-1047-4d5b-b03e-564fc2aea133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 56,
                "y": 19
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "73c8e9b8-02d8-4d24-ad60-980b77fdb9a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ef6cc9ce-570b-4e2a-8416-55abd194304d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2771ca94-9bcd-485c-9c20-24eedef9b78f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "90fc93e2-cc97-4f30-8f71-493a0e4f4370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "85659e8b-d7ed-4106-a1a5-c4c81d5b8bab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 3,
                "shift": 9,
                "w": 2,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "14e2c48c-82c6-47bc-bc05-ce8648699c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "cb1c8ae2-b4ae-4158-99e0-1403707052bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "36c5f981-111b-4f75-8409-69cbab086bf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "3a09808d-6235-47ad-a21e-84e245d00e28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "17bf6075-2196-45d8-b4fd-fa07d23436a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8f9c9527-411d-4523-9202-766fdf77ca33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "067a35ba-69d2-44dc-93f4-da0c8b8d3199",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 71,
                "y": 19
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b6c0ce4e-576f-4589-859c-36311390e614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 36
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ea393d9e-f91b-44a1-a156-5bb541c81651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 80,
                "y": 19
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b25002cf-3f80-43e2-93af-3f776b75ab49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 4,
                "x": 115,
                "y": 36
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c5ff842d-9ffa-451d-9525-07fff6a09bc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 107,
                "y": 36
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "26b4b67a-1d83-4aec-b09e-9b431e344a05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 100,
                "y": 36
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7ddf5bed-7858-46e6-830d-0f065fa20d39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 91,
                "y": 36
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "468889d2-58ee-472e-bc1b-0826ae3750b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 82,
                "y": 36
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3b1e8691-be68-4047-89ea-96f43af0a554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 73,
                "y": 36
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4d7c9c1f-0dbe-48f8-b0cd-5421f384a90f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 64,
                "y": 36
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ea3a764d-06d1-4716-ac52-3099c8101d18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 55,
                "y": 36
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "85f070fa-ee5b-4bc6-b99d-9431566ef7b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 47,
                "y": 36
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5f56f13a-f3c4-4ffd-928d-1fe0b3bfcf09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "77256999-d7d9-4363-b50b-3c18106c84a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 36
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b911f503-5050-4217-8311-153c00634c56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 36
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "65d0909c-1ab7-4a51-aa5a-ff72b350a093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 36
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3ab919d7-4523-464e-8483-4dc2e47a7d34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c914449f-f125-46d8-9530-718b6f8b8f67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 118,
                "y": 19
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "3ca394d8-5dfe-4fb1-b52c-b61bd58da502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 109,
                "y": 19
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0b6c10dc-d700-4125-961c-d2255cd66530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 100,
                "y": 19
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "eefc12ec-6519-4403-8286-fd89def7fbd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 95,
                "y": 19
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "184b7d7f-c3b9-4ced-a795-f41c32a6de7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 4,
                "shift": 9,
                "w": 1,
                "x": 92,
                "y": 19
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "35b3f125-2d2a-45cc-8b10-c18e3ad64111",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 87,
                "y": 19
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b21329c3-b491-437b-9646-12c5fb1ab650",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 14,
                "y": 104
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "2a0ae6f1-a909-4418-af64-e756b93445c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 15,
                "offset": 3,
                "shift": 16,
                "w": 9,
                "x": 22,
                "y": 104
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}