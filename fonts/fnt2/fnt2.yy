{
    "id": "6927cc0b-b3ec-434f-99af-b1e5d6486fef",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt2",
    "AntiAlias": 0,
    "TTFName": "${project_dir}\\fonts\\fnt2\\SmallBugMonospac.ttf",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "SmallBugMonospac",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b7005191-3efa-4882-ad5b-79df7eb7272d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d6720537-81fc-4182-9daa-e93ad73bac8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 30,
                "offset": 6,
                "shift": 18,
                "w": 2,
                "x": 206,
                "y": 98
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "97af9df6-edea-465f-a983-13edf9903400",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 8,
                "x": 196,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "32ac0e5d-e4ff-47cc-9211-0a09eac4f7fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 178,
                "y": 98
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "56df6e4b-c718-4aea-b3f1-56b82e868d8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 162,
                "y": 98
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "19234a52-e78e-4593-ab5f-7ef84e2bdc51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 146,
                "y": 98
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "06cf01cd-c709-412c-9c91-f8627ef5d28d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 12,
                "x": 132,
                "y": 98
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4278b5ef-9348-40ab-846d-42f3a7b91a6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 30,
                "offset": 8,
                "shift": 18,
                "w": 2,
                "x": 128,
                "y": 98
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5ae3bcc5-e1f2-4e2c-9052-528f5cb6d4a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 8,
                "x": 118,
                "y": 98
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "315c3cdc-b244-4704-bacb-a6a64802c511",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 30,
                "offset": 6,
                "shift": 18,
                "w": 8,
                "x": 108,
                "y": 98
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "edca818b-6a6a-43ca-86e8-2668e6beda13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 12,
                "x": 210,
                "y": 98
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c3acbd3a-865d-4c60-9fce-f8b7c11ed10f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 92,
                "y": 98
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9e20a023-339b-4b5f-af59-51c0e3e7beff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 4,
                "x": 72,
                "y": 98
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "7b386792-8765-49e8-83d9-6700384cc8a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 10,
                "x": 60,
                "y": 98
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7ec0fb26-c93f-43f7-b7dc-31fc42832b10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 4,
                "x": 54,
                "y": 98
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a65b69ba-ebe3-4590-a1bd-17b1da0ab2ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 10,
                "x": 42,
                "y": 98
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b06ee9ba-2c70-49ac-a5b8-7c6de21fcbd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 12,
                "x": 28,
                "y": 98
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "7d6f0e89-5647-4b8e-ba0f-80b1bc32b855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 10,
                "x": 16,
                "y": 98
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "cd6db79e-0a29-406f-905c-2cde528fd723",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 12,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c4eb2700-8d6e-45fd-88f7-4ff9a4e19b7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 12,
                "x": 228,
                "y": 66
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7249a489-526d-4a48-bcee-e43ebcf90721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 212,
                "y": 66
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "022cd678-10d1-47bb-ac48-570335a09a11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 12,
                "x": 78,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "2bc539a0-1269-4941-9fee-80162ca8d6dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 12,
                "x": 224,
                "y": 98
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "60caa00a-6df3-4ffc-9db8-d6796ea0c66b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 238,
                "y": 98
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "041158cb-5bd4-4dbb-9cb1-be482586a426",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 12,
                "x": 2,
                "y": 130
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8015f034-c623-459e-b1b1-e1ae405a082f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 12,
                "x": 54,
                "y": 162
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "11fcbf2f-538e-440c-8f81-1675741b650a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 4,
                "x": 48,
                "y": 162
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ac427fde-43e3-45dd-b068-a8b510445664",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 4,
                "x": 42,
                "y": 162
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4dade1d5-d1e8-4258-b5ad-8f0893eb4ee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 12,
                "x": 28,
                "y": 162
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6ecebdc3-48a8-4fe2-be75-b0984b7a6443",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 10,
                "x": 16,
                "y": 162
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "c1d5431c-3e91-4ac8-9cbf-a902fe561e52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 12,
                "x": 2,
                "y": 162
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7876db83-89a0-4ad2-b5c1-7fbcedaed9f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 10,
                "x": 238,
                "y": 130
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a071a100-9845-498e-ab4d-fb39f0907f30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 220,
                "y": 130
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "71f8b078-bffc-4ed3-94be-c80e99c33b20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 204,
                "y": 130
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ee2ad0ec-1a13-4fd5-803c-b0807b8689d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 188,
                "y": 130
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "dac79249-565a-4ab0-b778-e72bd7bb5196",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 172,
                "y": 130
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "355d39c1-9bd2-4caf-94dd-ac772bd8d08c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 156,
                "y": 130
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "68942ff9-f871-4751-9299-8abf4276c657",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 140,
                "y": 130
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "61d5c272-7581-4661-a93f-58742886248d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 124,
                "y": 130
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "faa37a59-1ac9-4a12-9909-43f40700041f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 108,
                "y": 130
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "70200f89-3fef-438e-a961-a2332da84916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 92,
                "y": 130
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5525feeb-336e-4faa-93b3-f0b6a312b6b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 10,
                "x": 80,
                "y": 130
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e1df12c5-63d4-4db4-9687-61957bbedad0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 64,
                "y": 130
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5a57fcfd-52a8-4c46-913f-1e3c2ccded7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 48,
                "y": 130
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "03ae14aa-8703-4ccb-94c1-01961b49ca2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 32,
                "y": 130
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "53c0e646-b97d-4787-8cfb-9de54d43ace1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 16,
                "y": 130
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2ea1dc7d-f0cc-4930-bf22-4f26339bfcc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 196,
                "y": 66
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "8afce1e8-0f90-486d-8f73-37c91c815a02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 180,
                "y": 66
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6873c7c9-f20d-4170-b0e1-3fbc2ed9bd06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 164,
                "y": 66
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "04f49acf-b9f6-4f4d-b2bc-90d8a27ae536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 76,
                "y": 34
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cd80b5a1-81c7-46b9-b58c-d37f9e10f13c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 50,
                "y": 34
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e1cd7372-625f-41ea-a093-e1261d316785",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 34,
                "y": 34
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c265fc5a-cb76-48a0-9717-dcd05c4d9748",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 18,
                "y": 34
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "2a45f43d-ad6d-4f79-9654-99fbcb0f3814",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f4c9108f-8883-4011-b350-346a1eae2626",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5571b3d2-2961-48a4-8555-b8f2d90c5c6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "eb2705b6-b427-4cea-b2ad-74fe67217554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "25d9b526-96ae-49f2-98a1-87bea9b682c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "70b3d59d-a431-46a5-b6f4-164db94e41d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "61d72f5f-b058-4390-924f-7cdba9ac411f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 8,
                "x": 66,
                "y": 34
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4bc23a37-0f45-4ab4-80c3-91da35efec61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 10,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5e3222ff-0d93-4064-a5a0-b179af1ae515",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 30,
                "offset": 6,
                "shift": 18,
                "w": 8,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7b4ffd17-633c-49c4-bca6-1bfc3e2d3dcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 10,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a45ea674-aac1-4a63-a0d5-5f24004369e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2f392587-e385-455e-b765-014f7b81693f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 30,
                "offset": 6,
                "shift": 18,
                "w": 4,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8f6737a1-1dee-4012-a1c1-3253592a1111",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b60838f2-ee74-435a-92ca-2c711a24dc51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "39f19203-3b11-42b7-b06d-5924ae3e98f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ba3f6bc5-96fd-4020-a0c6-43e9b53f2445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7a6b6e04-fa80-4065-808d-47ea98955cf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bbe2965b-6f07-402f-b759-3832126d5cae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c4bb4e16-5d65-4ef8-8637-4931b5400923",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 92,
                "y": 34
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "6ecc9eb2-a1e0-407e-a95a-9f7a58513cbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 236,
                "y": 34
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9a4125b1-2142-4996-9f03-c02fb046ea2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 10,
                "x": 108,
                "y": 34
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "c0720555-f99a-4754-8fbc-414aec44a2e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 8,
                "x": 138,
                "y": 66
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "233c7a41-af2b-46ed-939b-f04d317d292c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 12,
                "x": 124,
                "y": 66
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "fd8948b0-1903-44a8-bb4b-895a162043ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 10,
                "x": 112,
                "y": 66
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dba5a03e-bf6f-4348-9e56-7aa3ad043c40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 96,
                "y": 66
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "acbaa5ed-a2fc-4800-bdf7-6b0e8068314a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 80,
                "y": 66
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d7cb86af-c59e-42a1-b3ad-3c40ee5c162d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 64,
                "y": 66
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "cc521519-9f83-4fca-8d6b-86db7115cba4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 48,
                "y": 66
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2f76c84f-af13-4210-b915-8ec0c1e058f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 32,
                "y": 66
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f72367f0-5a36-41f3-8435-ffb08e3ffd1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 30,
                "offset": 4,
                "shift": 18,
                "w": 12,
                "x": 18,
                "y": 66
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "cd08607a-5585-4ddf-9abd-7808276fa03d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 148,
                "y": 66
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ff54930b-6a3b-41d3-b963-4a9da6488d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3dfa5a97-b230-4fe5-86a1-265f808fd468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 220,
                "y": 34
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0f7a4216-9bdc-4ac8-8a0b-02f35a9c99f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 204,
                "y": 34
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e3e986c2-f044-4821-aace-c1b0cd1a383c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 188,
                "y": 34
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2ef9daf5-8cdb-4d46-9f47-b7db08f8e770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 172,
                "y": 34
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "985dbba2-db5e-4eac-8d9f-aa800f4e2a08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 156,
                "y": 34
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8f9558e6-9e9a-489a-a0f1-c413f9422564",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 140,
                "y": 34
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "54b74f25-5242-46d2-96f4-9cf0c826a166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 30,
                "offset": 6,
                "shift": 18,
                "w": 6,
                "x": 132,
                "y": 34
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c6e12d6b-e8e8-458d-95c6-5fa33f7ef245",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 30,
                "offset": 8,
                "shift": 18,
                "w": 2,
                "x": 128,
                "y": 34
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0736724c-c4d3-4ded-b2fe-9c90e54125da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 30,
                "offset": 6,
                "shift": 18,
                "w": 6,
                "x": 120,
                "y": 34
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6deabca0-54a4-430e-8be8-84eafd67bbc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 30,
                "offset": 2,
                "shift": 18,
                "w": 12,
                "x": 68,
                "y": 162
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "1ac0009c-6f86-44c1-8a47-187d07d24a33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 30,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 82,
                "y": 162
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}