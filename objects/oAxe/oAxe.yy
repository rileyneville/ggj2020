{
    "id": "f6ef6318-f320-4dc6-b7d9-48369579cf45",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAxe",
    "eventList": [
        {
            "id": "44e18b4f-0f44-4e2f-9f72-986da51ef792",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6ef6318-f320-4dc6-b7d9-48369579cf45"
        },
        {
            "id": "3b73fa5b-ff7f-4900-934e-8ab096e5523d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "f6ef6318-f320-4dc6-b7d9-48369579cf45"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "471b4585-2966-467b-b28e-eb5c532f5ff8",
    "visible": false
}