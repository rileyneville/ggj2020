target_x = x;
target_y = y;
last_moved_x = x;
last_moved_y = y;
camera_acc = 0.1;
global.subpixel_rendering = false;

zoom = 1;
last_zoom = zoom;
target_zoom = 1;
zoom_acc = 0.05;
game_scale = 1;
target_vres = 300;
target_gui_vres = 200;


camera = view_camera[0];
base_camera_h = view_hport[0];

window_w = window_get_width();
window_h = window_get_height();

gui_w = window_w;
gui_h = window_h;

window_ratio = window_w/window_h;
base_camera_w = base_camera_h*window_ratio;

camera_w = base_camera_w/zoom;
camera_h = base_camera_h/zoom;

left = camera_get_view_x(camera);
right = left + camera_w;
top = camera_get_view_y(camera);
bottom = top + camera_h;

small_pixel_blur = true;
lock_to_room = false;
reset_view = true;
global.fullscreen = window_get_fullscreen();

application_surface_draw_enable(false);

gamma = 1;
u_gamma = shader_get_uniform(sh_gamma,"gamma");

global.paused = false;
global.paused_sprite = noone;

audio_listener_set_orientation(0,0,0,1000,0,-1,0);
audio_set_master_gain(0,1);
audio_falloff_set_model(audio_falloff_exponent_distance);

locked = false;

shake_dir = 0;
shake_v = 0;
shake_offset = 0;