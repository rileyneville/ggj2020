/// @description Draw application layer with gamma


shader_set(sh_gamma);
shader_set_uniform_f(u_gamma,gamma);

if (global.paused) {
	if (sprite_exists(global.paused_sprite)) {
		if (global.subpixel_rendering) {
			draw_sprite(global.paused_sprite,0,0,0);
		} else {
			draw_sprite_stretched(global.paused_sprite,0,-game_scale-frac(x)*game_scale,-game_scale-frac(y)*game_scale,window_w+2*game_scale,window_h+2*game_scale);
		}
	}
} else {
	if (global.subpixel_rendering) {
		draw_surface(application_surface, 0, 0);
	} else {
		draw_surface_stretched(application_surface,-game_scale-frac(x)*game_scale,-game_scale-frac(y)*game_scale,window_w+2*game_scale,window_h+2*game_scale);
	}
}

shader_reset();
