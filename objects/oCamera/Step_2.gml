if ((window_get_height() != window_h || window_get_width() != window_w
		|| display_get_gui_height() != gui_h || display_get_gui_width() != gui_w || reset_view
		|| global.fullscreen != window_get_fullscreen()) 
		&& window_get_height() > 0 && window_get_width() > 0) {
	//unpause();
	reset_view = false;
	global.fullscreen = window_get_fullscreen();
	var new_window_w = window_get_width();
	var new_window_h = window_get_height();
	if (new_window_h > 0 && new_window_w > 0) {
		window_w = new_window_w;
		window_h = new_window_h;
		
		base_camera_h = new_window_h;
		game_scale = 1;
		repeat(4) {
			game_scale++;
			if (abs(base_camera_h-target_vres) > abs((new_window_h/game_scale)-target_vres)) {
				base_camera_h = floor(new_window_h/game_scale);
			} else {
				break;
			}
		}
		
		
		window_ratio = window_w/window_h;
		base_camera_w = floor(base_camera_h*window_ratio);
		if (!global.subpixel_rendering) {
			//base_camera_w+=2;
			//base_camera_h+=2;
		}
			
		
		view_set_hport(0,base_camera_h);
		view_set_wport(0,base_camera_w);
		view_set_camera(0,camera);
		
		//window_set_size(window_w,window_h);
		if (global.subpixel_rendering) {
			surface_resize(application_surface,window_w,window_h);
		} else {
			surface_resize(application_surface,base_camera_w,base_camera_h);
		}

		camera_w = base_camera_w/zoom;
		camera_h = base_camera_h/zoom;
		
		gui_h = new_window_h;
		var gui_scale = 1;
		repeat(4) {
			gui_scale++;
			if (abs(gui_h-target_gui_vres) > abs((new_window_h/gui_scale)-target_gui_vres)) {
				gui_h = floor(new_window_h/gui_scale);
			} else {
				break;
			}
		}
		gui_w = floor(gui_h*window_ratio);
		display_set_gui_size(gui_w,gui_h);
	}
	
}

if (!global.paused && !keyboard_check(ord("H"))) {

	if (lock_to_room) {
		target_x = clamp(target_x,camera_w/2,room_width-camera_w/2);
		//target_y = clamp(target_y,camera_h/2,room_height-camera_h/2);
		target_y = clamp(target_y,-100000,room_height-camera_h/2);//only lock in one direction. Allow for infinite sky.
		target_zoom = max(target_zoom,base_camera_h/room_height);
		target_zoom = max(target_zoom,base_camera_w/room_width);
	}
	
	if (!locked) {
		if (global.subpixel_rendering) {
			x = lerp(x,target_x,camera_acc);
			y = lerp(y,target_y,camera_acc);
			if (abs(frac((x-camera_w/2))-0.5) < 0.01) {
				x += 0.01;
			}
			if (abs(frac((y-camera_h/2))-0.5) < 0.01) {
				y += 0.01;
			}
		} else {
			var deadzone = 12;
			if (abs(x-target_x) > deadzone || abs(last_moved_x) >= 0.5) {
				x = lerp(x,target_x,camera_acc);
			}
		
			if (abs(y-target_y) > deadzone || abs(last_moved_y) >= 0.5) {
				y = lerp(y,target_y,camera_acc);
			}
		}
	}
			
	
		
	target_zoom = clamp(target_zoom,0.25,10);
	last_zoom = zoom;
	zoom = lerp(zoom,target_zoom,zoom_acc);
	base_camera_h = view_hport[0];

	camera_w = base_camera_w/zoom;
	camera_h = base_camera_h/zoom;
	if (lock_to_room) {
		x = clamp(x,camera_w/2,room_width-camera_w/2);
		//y = clamp(y,camera_h/2,room_height-camera_h/2);
		y = clamp(y,-100000,room_height-camera_h/2); //only lock in one direction. Allow for infinite sky.
	}
	shake_offset += shake_v;
	shake_v += shake_offset*(-0.65);
	shake_v *= 0.75;
	if (abs(shake_v) < 0.1 && abs(shake_offset) < 0.1) {
		shake_v = 0;
		shake_offset = 0
	}
	
	gamepad_set_vibration(0,abs(shake_v),abs(shake_v));

	
	shake_dir += random_range(15,15);
	var view_x = (x-camera_w/2) + lengthdir_x(shake_offset,shake_dir);
	var view_y = (y-camera_h/2) + lengthdir_y(shake_offset,shake_dir);
	if (!global.subpixel_rendering) {
		if (camera_w mod 2 == 1) {
			view_x = round(view_x);
		} else {
			view_x = floor(view_x);
		}
		if (camera_h mod 2 == 1) {
			view_y = round(view_y);
		} else {
			view_y = floor(view_y);
		}
	}
	camera_set_view_size(camera,camera_w,camera_h);
	camera_set_view_pos(camera,view_x,view_y);

	left = camera_get_view_x(camera);
	right = left + camera_w;
	top = camera_get_view_y(camera);
	bottom = top + camera_h;

	if (small_pixel_blur) {
		if (camera_w > window_w || camera_h > window_h) {
			gpu_set_tex_filter(true);
		} else {
			gpu_set_tex_filter(false);
		}
	}
}


last_moved_x = x-xprevious;
last_moved_y = y-yprevious;

if (instance_exists(oPlayer)) {
	audio_listener_set_position(0,oPlayer.x,oPlayer.y,-32);
} else {
	audio_listener_set_position(0,x,y,-32);
}

if (keyboard_check_pressed(ord("P"))) {
	//global.subpixel_rendering = !global.subpixel_rendering;
	reset_view = true;
}