/// @description
image_angle = choose(0,90,180,270);
falling = false;
fall_dir = 0;//random(360);
fall_spd = 0;
fall_acc = 0.0025;
fall_angle = 90;
height = 32;
trunk_height = 64;
roll_dir = random_sign();
transparency = 0;