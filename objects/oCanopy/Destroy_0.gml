/// @description
var trunk = instance_create_layer(x,y,"Building",oTreeTrunk);
trunk.image_angle = fall_dir;

for (var i = 0; i < image_number; i++) {
	var offset = lengthdir_x(trunk_height+(height/image_number)*(i+1),fall_angle);
	var xx = x+lengthdir_x(offset,fall_dir);
	var yy = y+lengthdir_y(offset,fall_dir);
	repeat(10) {
		var px = xx + random_range(-16,16);
		var py = yy + random_range(-16,16);
		part_particles_create(global.ps_floor,px,py,global.pt_foliage,1);
	}
}