/// @description

if (falling && sprite_index == sCanopyParts) {
	image_angle = rlerp(image_angle,fall_dir,0.05);
	var offset = lengthdir_x(trunk_height,fall_angle);
	var scale = 1-lengthdir_x(0.25,fall_angle);
	draw_sprite_ext(sTreeTrunk,0,x,y,offset/64,1,fall_dir,c_white,1);
	for (var i = 0; i < image_number; i++) {
		var offset = lengthdir_x(trunk_height+(height/image_number)*(i+1),fall_angle);
		draw_sprite_ext(sprite_index,i,x+lengthdir_x(offset,fall_dir),y+lengthdir_y(offset,fall_dir),
		0.5+0.5*(fall_angle/90),scale,image_angle,c_white,1);
	}
} else {
	image_index = transparency;
	draw_self();
}