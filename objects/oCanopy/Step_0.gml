/// @description
if (falling) {
	if ((collision_line(x,y,
		x+lengthdir_x(lengthdir_x(trunk_height+height,fall_angle),fall_dir),
		y+lengthdir_y(lengthdir_y(trunk_height+height,fall_angle),fall_dir),
		oSolid,true,true) != noone)
		|| (collision_line(x,y,
		x+lengthdir_x(lengthdir_x(trunk_height+height,fall_angle),fall_dir),
		y+lengthdir_y(lengthdir_y(trunk_height+height,fall_angle),fall_dir),
		oPlankMarker,true,true) != noone)) {
		
		fall_angle += roll_dir;
	}
	transparency = lerp(transparency,0,0.25);
	if (transparency <= 0.1) {
		sprite_index = sCanopyParts;
	}
	if (sprite_index == sCanopyParts) {
		fall_angle -= fall_spd;
		fall_spd += fall_acc;
		fall_acc *= 1.05;
		if (fall_angle <= 0) {
			if (rectangle_in_rectangle(bbox_left,bbox_top,bbox_right,bbox_bottom,oCamera.left,oCamera.top,oCamera.right,oCamera.bottom)) {
				shake_screen(1);
			}
			
			audio_play_sound_at(aTreeLand,x+lengthdir_x(trunk_height,fall_dir),y+lengthdir_y(trunk_height,fall_dir),0,128,512,1,false,10);
			instance_destroy();			  
		}
	}
} else {
	var col = false;
	with (oPlayer) {
		if (place_meeting(x,y,other)) {
			col = true;
		}
	}
	if (col) {
		transparency = lerp(transparency,4.5,0.1);
	} else {
		transparency = lerp(transparency,0,0.1);
	}
}