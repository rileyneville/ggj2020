{
    "id": "4c8a2670-07d0-43d2-83fd-024b38da207c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCanopy",
    "eventList": [
        {
            "id": "34f7dc45-3b3b-421b-ad7f-1818d718fc16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4c8a2670-07d0-43d2-83fd-024b38da207c"
        },
        {
            "id": "d6eeeddf-8dee-45b4-ac71-0dda29b04af6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4c8a2670-07d0-43d2-83fd-024b38da207c"
        },
        {
            "id": "6db4bb54-4419-4c81-8b23-fbb753c07bfe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4c8a2670-07d0-43d2-83fd-024b38da207c"
        },
        {
            "id": "c7ff9e2d-0963-410d-a0a4-2d7829a54ae6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "4c8a2670-07d0-43d2-83fd-024b38da207c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e9097b78-36e4-4c53-8c0a-1748d35f06f9",
    "visible": true
}