/// @description hatch
instance_create_depth(x,y,depth-10,oTermite);

part_type_direction(global.pt_eggshell,0,360,0,0);
repeat(10) {
	var xx = random_range(bbox_left,bbox_right);
	var yy = random_range(bbox_top,bbox_bottom);
	part_particles_create(global.ps_floor,xx,yy,global.pt_eggshell,1);
}
instance_destroy();
