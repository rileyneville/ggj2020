/// @description
x += hsp;
y += vsp;
hsp = lerp(hsp,0,drag);
vsp = lerp(vsp,0,drag);
image_xscale += scalev;
image_yscale += scalev;
scalev = lerp(scalev,1-image_xscale,0.25);
if (alarm[0] < 60) {
	if (alarm[0] mod 10 == 0) {
		image_angle = random_range(30,30);
		hsp = random_range(-1,1);
		vsp = random_range(-1,1);
		drag = 0.25;
	}
}