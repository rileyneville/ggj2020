{
    "id": "78c2965e-0789-494b-a006-1c0e7f7ebc13",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEgg",
    "eventList": [
        {
            "id": "d7fa301d-de67-4ac7-a809-e6474c8bcc39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "78c2965e-0789-494b-a006-1c0e7f7ebc13"
        },
        {
            "id": "6a65ea36-02a4-4591-ab83-8b3be1875c91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "78c2965e-0789-494b-a006-1c0e7f7ebc13"
        },
        {
            "id": "ad3e5373-8b6a-44f2-ab09-50abfd120077",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "78c2965e-0789-494b-a006-1c0e7f7ebc13"
        },
        {
            "id": "ebd275ad-21b1-455d-b017-217f09d973f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "78c2965e-0789-494b-a006-1c0e7f7ebc13"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1b0f36a2-f0f8-4276-bf6b-f4a0145d3658",
    "visible": true
}