{
    "id": "9a14bf78-150b-437a-a29c-0b9c3ee54342",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFadeIn",
    "eventList": [
        {
            "id": "36d127e3-4a79-49b0-ad4f-f2c8f04ce084",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9a14bf78-150b-437a-a29c-0b9c3ee54342"
        },
        {
            "id": "e88abcb9-022f-464f-8ab1-3998dd6ffbba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9a14bf78-150b-437a-a29c-0b9c3ee54342"
        },
        {
            "id": "f964dab7-5346-4e5f-b7a8-574603888b8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "9a14bf78-150b-437a-a29c-0b9c3ee54342"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}