/// @description
alarm[1] = 60;
var wood_needed = 0;
var wood_exists = 0;
with (oPlankMarker) {
	wood_needed ++;
}
with (oPlank) {
	wood_exists++;
}
with (oTree) {
	wood_exists += 4;
}
with (oTreeTrunk) {
	wood_exists += 4;
} 
with (oLog) {
	wood_exists ++;
}
with (oPlayer) {
	if (carrying_log) {
		wood_exists++;
	}
}

if (wood_exists < wood_needed) {
	instance_create_depth(x,y,depth,oLevelFailed);
}