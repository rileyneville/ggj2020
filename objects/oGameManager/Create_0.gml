/// @description
global.screenshake = 3;
draw_set_font(fnt1);
global.ps_floor = part_system_create();
global.ps_top = part_system_create();
part_system_depth(global.ps_floor,layer_get_depth("BGSprites"));
global.pt_splinter = part_type_create();
part_type_sprite(global.pt_splinter,sSplinter,false,false,true);
part_type_alpha3(global.pt_splinter,1,1,0);
part_type_speed(global.pt_splinter,0.5,2,-0.05,0);
part_type_direction(global.pt_splinter,0,360,0,0);
part_type_orientation(global.pt_splinter,0,360,0,0,0);
part_type_life(global.pt_splinter,360,600);


global.pt_eggshell = part_type_create();
part_type_sprite(global.pt_eggshell,sEggShell,false,false,true);
part_type_alpha3(global.pt_eggshell,1,1,0);
part_type_speed(global.pt_eggshell,0.5,2,-0.05,0);
part_type_direction(global.pt_eggshell,0,360,0,0);
part_type_orientation(global.pt_eggshell,0,360,0,0,0);
part_type_life(global.pt_eggshell,360,600);


global.pt_termite_parts = part_type_create();
part_type_sprite(global.pt_termite_parts,sTermiteParts,false,false,true);
part_type_alpha3(global.pt_termite_parts,1,1,0);
part_type_speed(global.pt_termite_parts,0.5,2,-0.05,0);
part_type_direction(global.pt_termite_parts,0,360,0,0);
part_type_orientation(global.pt_termite_parts,0,360,0,0,0);
part_type_life(global.pt_termite_parts,360,600);

global.pt_foliage = part_type_create();
part_type_sprite(global.pt_foliage,sFoliageBunches,false,false,true);
part_type_alpha3(global.pt_foliage,1,1,0);
part_type_speed(global.pt_foliage,0.5,2,-0.1,0);
part_type_direction(global.pt_foliage,0,360,0,0);
part_type_orientation(global.pt_foliage,0,360,0,0,0);
part_type_life(global.pt_foliage,360,600);

global.pt_footstep = part_type_create();
part_type_sprite(global.pt_footstep,sFootstep,false,false,true);
part_type_alpha3(global.pt_footstep,1,1,0);
part_type_speed(global.pt_footstep,0,0,0,0);
part_type_direction(global.pt_footstep,0,360,0,0);
part_type_orientation(global.pt_footstep,0,360,0,0,0);
part_type_life(global.pt_footstep,360,600);


global.pt_sparkle = part_type_create();
part_type_sprite(global.pt_sparkle,sSparkle,false,false,true);
part_type_alpha3(global.pt_sparkle,0,1,0);
part_type_speed(global.pt_sparkle,0,1,-0.05,0);
part_type_direction(global.pt_sparkle,0,360,0,0);
part_type_orientation(global.pt_sparkle,0,360,0,5,0);
part_type_life(global.pt_sparkle,60,120);

global.master_gain = 1;
global.music_gain = 1;
ambience = audio_play_sound(aForestAmbience,10,1);
music =  audio_play_sound(aForestCabin,10,1);
audio_set_master_gain(0,global.master_gain);
audio_sound_gain(music,global.music_gain*0.25,0);
alarm[1] = 60;