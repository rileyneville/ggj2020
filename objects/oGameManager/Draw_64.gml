/// @description
draw_set_alpha(0.5);
draw_set_colour(c_black);
draw_rectangle(0,0,50,50,false);

draw_set_alpha(1);
draw_set_colour(c_white);
//draw_rectangle(0,0,50,50,true);
draw_sprite(sPixel,0,25,25);
with (oPlankMarker) {
	var xx = (x-oPlayer.x)/32;
	var yy = (y-oPlayer.y)/32;
	if (abs(xx) < 25 && abs(yy) < 25) {
		if (instance_exists(plank)) {
			draw_sprite_ext(sPixel,0,25+xx,25+yy,1,1,0,c_green,1);
		} else {
			draw_sprite_ext(sPixel,0,25+xx,25+yy,1,1,0,c_orange,1);
		}
			
	}
}
with (oTermite) {
	var xx = (x-oPlayer.x)/32;
	var yy = (y-oPlayer.y)/32;
	if (abs(xx) < 25 && abs(yy) < 25) {
		draw_sprite_ext(sPixel,0,25+xx,25+yy,1,1,0,c_red,1);
	}
}

with (oTree) {
	var xx = (x-oPlayer.x)/32;
	var yy = (y-oPlayer.y)/32;
	if (abs(xx) < 25 && abs(yy) < 25) {
		draw_sprite_ext(sPixel,0,25+xx,25+yy,1,1,0,c_yellow,1);
	}
}

var wood_needed = 0;
var wood_exists = 0;
with (oPlankMarker) {
	wood_needed ++;
}
with (oPlank) {
	wood_exists++;
}

var repaired = 100*(wood_exists/wood_needed);
draw_healthbar(60,5,display_get_gui_width()-60,10,repaired,c_black,c_red,c_green,0,true,false);
var text = string(round(repaired)) + "%";
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_colour(c_black);
draw_text(display_get_gui_width()/2+1,6,text);
draw_set_colour(c_white);
draw_text(display_get_gui_width()/2,5,text);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);

var sy = 10;
var sx = display_get_gui_width()-40
var col = c_ltgray;
if (point_distance(sx,sy,guix(mouse_x),guiy(mouse_y)) < 8) {
	col = c_white;
	if (mouse_check_button_pressed(1)) {
		global.master_gain = !global.master_gain;
		audio_set_master_gain(0,global.master_gain);
	}
}
draw_sprite_ext(sSound,global.master_gain,sx,sy,1,1,0,col,1);


var sx = display_get_gui_width()-20
var col = c_ltgray;
if (point_distance(sx,sy,guix(mouse_x),guiy(mouse_y)) < 8) {
	col = c_white;
	if (mouse_check_button_pressed(1)) {
		global.music_gain = !global.music_gain;
		audio_sound_gain(music,global.music_gain*0.25,250);
	}
}
draw_sprite_ext(sMusic,global.music_gain,sx,sy,1,1,0,col,1);