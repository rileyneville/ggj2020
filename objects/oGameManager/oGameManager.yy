{
    "id": "c9628841-0156-4b13-a552-57962e2255aa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameManager",
    "eventList": [
        {
            "id": "fdc481c1-0078-4705-bedb-6adbf000eaf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c9628841-0156-4b13-a552-57962e2255aa"
        },
        {
            "id": "0b3e3739-a85b-40d9-884b-26bf30ec0877",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c9628841-0156-4b13-a552-57962e2255aa"
        },
        {
            "id": "6d297e72-c4e5-4b63-98cf-f0e3db301f7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c9628841-0156-4b13-a552-57962e2255aa"
        },
        {
            "id": "124915a4-3ee2-4f22-ba42-a450fb636480",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c9628841-0156-4b13-a552-57962e2255aa"
        },
        {
            "id": "e514bf98-60a2-4525-8646-66594c23e42a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "c9628841-0156-4b13-a552-57962e2255aa"
        },
        {
            "id": "6d846d07-30f2-4d30-bb1e-55b230517877",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "c9628841-0156-4b13-a552-57962e2255aa"
        },
        {
            "id": "4ec62446-9651-45c2-83ec-38ce0de8205b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "c9628841-0156-4b13-a552-57962e2255aa"
        },
        {
            "id": "8798dfbc-c5a4-4ae7-a61d-e0c7d65d5c59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "c9628841-0156-4b13-a552-57962e2255aa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}