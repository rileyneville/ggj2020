/// @description
with (oTermite) {
	part_type_direction(global.pt_termite_parts,0,360,0,0);
	repeat(10) {
		var xx = random_range(bbox_left,bbox_right);
		var yy = random_range(bbox_top,bbox_bottom);
		part_particles_create(global.ps_floor,xx,yy,global.pt_termite_parts,1);
	}
	instance_destroy();
}
radius = 10;
plank_list = ds_list_create();
with (oPlank) {
	ds_list_add(other.plank_list,id);
}
depth = oGameManager.depth-1;
visible = true;