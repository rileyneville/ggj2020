/// @description
gpu_set_blendmode(bm_add);
draw_set_alpha(0.25);
draw_circle_color(x,y,radius,c_black,c_white,false);
draw_set_alpha(1);
gpu_set_blendmode(bm_normal);