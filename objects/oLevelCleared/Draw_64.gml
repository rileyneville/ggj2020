/// @description
draw_set_colour(c_black);
if (alarm[0] > 0) {	
	draw_set_alpha(clamp(alarm[0]/60,0,1));
} else {
	draw_set_alpha(clamp(radius/100,0,1));
}
if (room_exists(room_next(room))) {
	var txt = "ALL FIXED!";
} else {
	var txt = "CONGRATS!\nYOU WON!";
}
	
draw_set_font(fnt2);
draw_text(display_get_gui_width()/2+1,display_get_gui_height()/2+1,txt);

draw_set_colour(c_white);
draw_text(display_get_gui_width()/2,display_get_gui_height()/2,txt);

draw_set_font(fnt1);

if (alarm[0] > 0 && room_exists(room_next(room))) {
	draw_set_colour(c_black);
	draw_set_alpha(1-clamp(alarm[0]/60,0,1));
	draw_rectangle(0,0,display_get_gui_width(),display_get_gui_height(),false);
	draw_set_colour(c_white);
}

draw_set_alpha(1);