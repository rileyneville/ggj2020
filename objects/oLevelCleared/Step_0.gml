/// @description
var i = 0;
while (i < ds_list_size(plank_list)) {
	var p = plank_list[|i];
	if (point_distance(x,y,p.x,p.y) < radius) {
		with (p) {
			repeat(5) {
				var xx = random_range(bbox_left,bbox_right);
				var yy = random_range(bbox_top,bbox_bottom);
				part_particles_create(global.ps_top,xx,yy,global.pt_sparkle,1);
			}
		}
		ds_list_delete(plank_list,i);
	} else {
		i++;
	}
}
radius *= 1.1;
if (ds_list_empty(plank_list) && alarm[0] <= 0) {
	alarm[0] = 180;
}