{
    "id": "85a80ba8-763e-45d7-bef6-df5a74295a2c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLevelCleared",
    "eventList": [
        {
            "id": "3bbdd430-349e-47af-bc4e-466c4ee188eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "85a80ba8-763e-45d7-bef6-df5a74295a2c"
        },
        {
            "id": "8c76e128-ce1e-46a8-8624-a15cfe80f076",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "85a80ba8-763e-45d7-bef6-df5a74295a2c"
        },
        {
            "id": "3f373ab0-fb76-4937-a75e-74f2d94e4e48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "85a80ba8-763e-45d7-bef6-df5a74295a2c"
        },
        {
            "id": "563b88c1-924b-4691-9fb8-a07dfb54d70b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "85a80ba8-763e-45d7-bef6-df5a74295a2c"
        },
        {
            "id": "13245c20-6247-4b58-acc8-5778e3bb432e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "85a80ba8-763e-45d7-bef6-df5a74295a2c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}