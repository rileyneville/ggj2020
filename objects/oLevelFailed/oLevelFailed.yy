{
    "id": "2a4b8a72-6ae3-4f3e-bcfc-52a34f8d4736",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLevelFailed",
    "eventList": [
        {
            "id": "78ffc350-afbe-42af-9c6e-794abd741daa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2a4b8a72-6ae3-4f3e-bcfc-52a34f8d4736"
        },
        {
            "id": "e312cced-38cc-4699-8629-9b74c7f59828",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2a4b8a72-6ae3-4f3e-bcfc-52a34f8d4736"
        },
        {
            "id": "f440ba1e-0ec9-4e3a-a278-c3e55abee8cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2a4b8a72-6ae3-4f3e-bcfc-52a34f8d4736"
        },
        {
            "id": "c0d4fc62-e276-4f22-81a1-7058cd01454d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "2a4b8a72-6ae3-4f3e-bcfc-52a34f8d4736"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}