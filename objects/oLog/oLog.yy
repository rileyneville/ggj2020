{
    "id": "5f7a87aa-71f9-4f7a-b826-422250dc65f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLog",
    "eventList": [
        {
            "id": "67b584d6-6aa8-40f5-963a-544e43f2505f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5f7a87aa-71f9-4f7a-b826-422250dc65f7"
        },
        {
            "id": "607e3234-6a09-48f5-b4ad-53d6178bf0c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5f7a87aa-71f9-4f7a-b826-422250dc65f7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a3fff901-09c7-4129-a881-b856b3460eb4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e14bc9f5-5b96-44fd-a5e5-7e968dec95f4",
    "visible": true
}