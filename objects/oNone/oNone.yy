{
    "id": "8ab65929-f5f9-4251-a04c-3f88168f8261",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oNone",
    "eventList": [
        {
            "id": "3d124cea-c202-4029-8393-66c8e34e5f07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "8ab65929-f5f9-4251-a04c-3f88168f8261"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f15576ee-ab80-4f4f-98d9-b2e816367ef7",
    "visible": true
}