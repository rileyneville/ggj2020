{
    "id": "b1c2c151-22a0-4001-97fc-5d30a117f1b8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlank",
    "eventList": [
        {
            "id": "4fd8bd52-12f6-41f3-834b-c769377c4314",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b1c2c151-22a0-4001-97fc-5d30a117f1b8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a3fff901-09c7-4129-a881-b856b3460eb4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7e5fde26-f546-4503-a6f1-17ac64e7e138",
    "visible": true
}