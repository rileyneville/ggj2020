/// @description
plank = noone;
if (irandom(1)) {
	plank = instance_create_depth(x,y,depth-1,oPlank);
	plank.image_angle = image_angle;
}
visible = false;
image_alpha = 0.5;
image_blend = c_red;