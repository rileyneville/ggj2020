{
    "id": "026355c8-a9ad-419a-8fb8-400f8ef79410",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlankMarker",
    "eventList": [
        {
            "id": "968883b6-5b5b-4035-a786-dc6bd01c4188",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "026355c8-a9ad-419a-8fb8-400f8ef79410"
        },
        {
            "id": "74233e73-4172-49ae-8242-c972ab775d2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "026355c8-a9ad-419a-8fb8-400f8ef79410"
        },
        {
            "id": "bcc61b40-de6f-40f6-bd99-9895816f2815",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "026355c8-a9ad-419a-8fb8-400f8ef79410"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bced557e-b742-4db1-b220-f3eacf422a1b",
    "visible": true
}