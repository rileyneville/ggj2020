/// @description
hsp = 0;
vsp = 0;
spd = 2;
acc = 0.15;
tdir = 0;
legs_frame = 0;
ldir = tdir;

lshoulder_x = x+lengthdir_x(8,tdir-90);
lshoulder_y = y+lengthdir_y(8,tdir-90);
lhand_offset = 12;
lhand_dir = 1;
							  
rshoulder_x = x+lengthdir_x(8,tdir+90);
rshoulder_y = y+lengthdir_y(8,tdir+90);

rhand_offset = 0;
rhand_dir = 1;

axe = instance_create_depth(x,y,depth+1,oAxe);
swing_timer = 0;
swing_speed = 0;
pow = 0;

last_step = 0;
carrying_log = false;
logx = 0;
logy = 0;
logangle = 0;
oCamera.x = x;
oCamera.y = y;
oCamera.target_x = x;
oCamera.target_y = y;