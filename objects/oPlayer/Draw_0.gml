/// @description
draw_sprite_ext(sPlayerLegs,legs_frame,x,y,1,1,ldir,c_white,1);
with (axe) {
	
	
	var lhandx = x + xoff(0,-other.lhand_offset);
	var lhandy = y + yoff(0,-other.lhand_offset);
	var larmdir = point_direction(other.lshoulder_x,other.lshoulder_y,lhandx,lhandy);
	var larmdist = point_distance(other.lshoulder_x,other.lshoulder_y,lhandx,lhandy);
	
	var rhandx = x + xoff(0,-other.rhand_offset);
	var rhandy = y + yoff(0,-other.rhand_offset);
	var rarmdir = point_direction(other.rshoulder_x,other.rshoulder_y,rhandx,rhandy);
	var rarmdist = point_distance(other.rshoulder_x,other.rshoulder_y,rhandx,rhandy);
	if (!other.carrying_log) {
		draw_sprite_ext(sPlayerArm,0,other.lshoulder_x,other.lshoulder_y,larmdist/12,other.lhand_dir,larmdir,c_white,1);
	
		draw_self();
	
		draw_sprite_ext(sPlayerHand,0,lhandx,lhandy,1,1,image_angle,c_white,1);
	} else {
		draw_self();
	}
	
	draw_sprite_ext(sPlayerHand,1,rhandx,rhandy,1,1,image_angle,c_white,1);
	draw_sprite_ext(sPlayerArm,0,other.rshoulder_x,other.rshoulder_y,rarmdist/12,other.rhand_dir,rarmdir,c_white,1);
}
draw_sprite_ext(sprite_index,image_index,x,y,1,1,tdir,c_white,1);

if (carrying_log) {
	draw_sprite_ext(sLog,0,logx,logy,1,1,logangle,c_white,1);
	draw_sprite_ext(sPlayerHand,1,logx+xoff(0,-8,logangle),logy+yoff(0,-8,logangle),1,1,logangle-90,c_white,1);
}