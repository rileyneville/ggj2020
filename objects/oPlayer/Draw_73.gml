/// @description
var toggle_log = keyboard_check_pressed(vk_space) || keyboard_check_pressed(ord("E")) || gamepad_button_check_pressed(0,gp_face1);
if (!carrying_log) {
	var log = instance_nearest(x,y,oLog);
	with (log) {
		if (distance_to_object(other) < 16) {
			draw_set_halign(fa_center);
			draw_set_valign(fa_middle);
			draw_sprite(sInteractable,0,x,y);
			if (toggle_log) {
				other.carrying_log = true;	
				other.logx = x;
				other.logy = y;
				other.logangle = image_angle;
				instance_destroy(id,false);
			}
		}
		
	}
} else {
	with (oPlankMarker) {
		if (!instance_exists(plank)) {
			if (distance_to_object(other) < 16) {
				draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,c_white,1);
				draw_set_halign(fa_center);
				draw_set_valign(fa_middle);
				draw_sprite(sInteractable,0,x,y);
				if (toggle_log) {
					toggle_log = false;
					other.carrying_log = false;
					plank = instance_create_depth(x,y,depth-1,oPlank);
					plank.image_angle = image_angle;
					audio_play_sound_at(choose(aPlaceWood1,aPlaceWood2,aPlaceWood3),x,y,0,64,256,1,false,10);
					var level_cleared = true;
					with (oPlankMarker) {
						if (!instance_exists(plank)) {
							level_cleared = false;
							break;
						}
					}
					if (level_cleared) {
						instance_create_depth(x,y,depth,oLevelCleared);	
					}
				}
				break;
			}
		}
	}
	if (toggle_log) {
		var log = instance_create_layer(x+xoff(8,-8,tdir),y+yoff(8,-8,tdir),"Building",oLog);
		log.image_angle = logangle;
		log.hsp = xoff(1,0,tdir);
		log.vsp = yoff(1,0,tdir);
		carrying_log = false;
	}
}