/// @description
gamepad_set_axis_deadzone(0,0.2);
mvh = clamp(keyboard_check(ord("D"))-keyboard_check(ord("A"))+
	keyboard_check(vk_right)-keyboard_check(vk_left)+
	gamepad_axis_value(0,gp_axislh),-1,1);
mvv = clamp(keyboard_check(ord("S"))-keyboard_check(ord("W"))+
	keyboard_check(vk_down)-keyboard_check(vk_up)+
	gamepad_axis_value(0,gp_axislv),-1,1);

mvdir = (point_direction(0,0,mvh,mvv));
mvh = min(abs(mvh),abs(lengthdir_x(1,mvdir)))*sign(mvh);
mvv = min(abs(mvv),abs(lengthdir_y(1,mvdir)))*sign(mvv);

hsp = lerp(hsp,mvh*spd,acc);
vsp = lerp(vsp,mvv*spd,acc);

var xmv = hsp;
while (abs(xmv) > 0) {
	var xmvstep = min(abs(xmv),1)*sign(xmv);
	if (!place_meeting(x+xmvstep,y,oSolid)) {
		x += xmvstep;
		xmv -= xmvstep;
	} else {
		hsp = 0.75*hsp;	
		xmv = 0;
	}	
}

var ymv = vsp;
while (abs(ymv) > 0) {
	var ymvstep = min(abs(ymv),1)*sign(ymv);
	if (!place_meeting(x,y+ymvstep,oSolid)) {
		y += ymvstep;
		ymv -= ymvstep;
	} else {
		vsp = 0.75*vsp;
		ymv = 0;
	}	
}
var mvd = 0;
while (place_meeting(x,y,oSolid) && mvd < 5) {
	mvd++;
	var s = instance_nearest(x,y,oSolid);
	var mdir = point_direction(s.x,s.y,x,y);
	x += lengthdir_x(1,mdir);
	y += lengthdir_y(1,mdir);
}

var cspd = point_distance(0,0,hsp,vsp);
if (cspd > 0.1){
	if (abs(angle_difference(mvdir,tdir)) <= 90) {
		ldir = rlerp(ldir,mvdir,0.2*cspd);
		legs_frame += 0.25*cspd;
	} else {
		ldir = rlerp(ldir,mvdir+180,0.2*cspd);
		legs_frame -= 0.25*cspd;
	}
	legs_frame = (legs_frame + 12) mod 12;
} else {
	ldir = rlerp(ldir,tdir,0.1);
	
	var lfd = (legs_frame/12)*360;
	if (abs(angle_difference(lfd,0)) <= 90) {
		lfd = rlerp(lfd,0,0.1);
	} else {
		lfd = rlerp(lfd,180,0.1);
	}
	legs_frame = (legs_frame/360)*12;
}

if (axe.swinging) {
	swing_timer--;
	if (swing_timer <= 0) {
		axe.swinging = false;
	}
}

if (mouse_check_button(1)) {
	tdir = rlerp(tdir,point_direction(x,y,mouse_x,mouse_y),0.25);
} else if (axe.swinging) {
	tdir -= swing_speed;
	swing_speed = lerp(swing_speed,0,0.1);
} else if (gamepad_axis_value(0,gp_axisrh) != 0 || gamepad_axis_value(0,gp_axisrv) != 0) {
	tdir = rlerp(tdir,point_direction(0,0,gamepad_axis_value(0,gp_axisrh), gamepad_axis_value(0,gp_axisrv)),0.25);
} else if (mvh != 0 || mvv != 0){
	tdir = rlerp(tdir,mvdir,0.1);
}


rshoulder_x = x+xoff(0,8,tdir);
rshoulder_y = y+yoff(0,8,tdir);

lshoulder_x = x+xoff(0,-8,tdir);
lshoulder_y = y+yoff(0,-8,tdir);

if (gamepad_button_check_released(0,gp_shoulderrb) ||gamepad_button_check_released(0,gp_shoulderr) || mouse_check_button_released(1)) {
	axe.swinging = true;
	swing_speed = 45*(swing_timer/15);
	with (axe) {
		last_x = x;
		last_y = y;
		last_angle = image_angle;
	}
}
if (mouse_check_button(1) || gamepad_button_check(0,gp_shoulderr) || gamepad_button_check(0,gp_shoulderrb)) {
	
	pow = (swing_timer/15);
	if (carrying_log) {
		swing_timer = min(swing_timer,5);
	}
	
	axe.swinging = false;
	swing_timer = min(swing_timer+0.5,15);
	var adir = tdir + 15*dsin((legs_frame/12)*360);
	
	rshoulder_x = lerp(rshoulder_x, x+xoff(2,8,tdir),pow);
	rshoulder_y = lerp(rshoulder_y, y+yoff(2,8,tdir),pow);
	
	lshoulder_x = lerp(lshoulder_x, x+xoff(2,-8,tdir),pow);
	lshoulder_y = lerp(lshoulder_y, y+yoff(2,-8,tdir),pow);
	
	var ax =  lerp(axe.x,lerp(rshoulder_x +  xoff(16,0,adir),lshoulder_x + xoff(6,-4,adir),pow),0.5);
	var ay =  lerp(axe.y,lerp(rshoulder_y +  yoff(16,0,adir),lshoulder_y + yoff(6,-4,adir),pow),0.5);
	axe.x = ax+hsp/2;
	axe.y = ay+vsp/2;

	axe.image_angle = rlerp(axe.image_angle,rlerp(adir,adir+100,pow),0.5);
	
	lhand_dir = 1;
	lhand_offset = lerp(12,16,pow);
} else if (axe.swinging) {
	lhand_dir = 1;
	lhand_offset = lerp(lhand_offset,3,0.75);
	var adir = tdir - 90;
	axe.x = lerp(axe.x,rshoulder_x + xoff(12,0,tdir),0.5)+hsp/2;
	axe.y = lerp(axe.y,rshoulder_y + yoff(12,0,tdir),0.5)+vsp/2;
	axe.image_angle = rlerp(axe.image_angle,adir,0.75);
	
	with (axe) {
		var x1 = last_x + xoff(7,-15,last_angle);
		var y1 = last_y + yoff(7,-15,last_angle);
		var x2 = x+xoff(7,-15);
		var y2 = y+yoff(7,-15);
		var hitables = [oSolid,oTermite,oEgg];
		for (var i = 0; i < array_length_1d(hitables);i++) {
			with (hitables[i]) {
				if (instance_place(x,y,other)) {
					ds_list_add(other.hit_list,id);
				} else if (collision_line(x1,y1,x2,y2,id,true,false) == id) {
					ds_list_add(other.hit_list,id);
				}
			}
		}
		
		var dmg = min(other.pow,0.1)+other.pow*4;
		for (var i = 0; i < ds_list_size(hit_list); i++) {
			var hit = hit_list[| i];
			if (instance_is_ancestor(hit,oSolid)) {
				other.swing_speed = -1*abs(other.swing_speed/4);
				if (instance_is_ancestor(hit,oWood)) {
					damage_wood(hit,x2,y2,image_angle,dmg);
				}
				shake_screen(other.pow,image_angle);
				other.pow = 0;
				if (!audio_is_playing(axe_hit_sound)) {
					axe_hit_sound = audio_play_sound_at(choose(aAxeHitWood_02,aAxeHitWood_03,aAxeHitWood_04),x2,y2,0,128,256,1,false,10);
				}
			} else if (instance_is_ancestor(hit,oTermite)) {
				var confirmed_hit = true;
				part_type_direction(global.pt_termite_parts,image_angle-90,image_angle+90,0,0);
				with (hit) {
					if (!place_meeting(x,y,target_plank)) {
						repeat(10) {
							var xx = random_range(bbox_left,bbox_right);
							var yy = random_range(bbox_top,bbox_bottom);
							part_particles_create(global.ps_floor,xx,yy,global.pt_termite_parts,1);
						}
						instance_destroy();
					} else {
						confirmed_hit = false;
					}
				}
					
				if (confirmed_hit) {
					other.swing_speed -= 5;
					shake_screen(other.pow/2,image_angle);
				}
				
			} else if (instance_is_ancestor(hit,oEgg)) {
				
				part_type_direction(global.pt_eggshell,image_angle-90,image_angle+90,0,0);
				with (hit) {
					repeat(10) {
						var xx = random_range(bbox_left,bbox_right);
						var yy = random_range(bbox_top,bbox_bottom);
						part_particles_create(global.ps_floor,xx,yy,global.pt_eggshell,1);
					}
					instance_destroy();
				}

				other.swing_speed -= 5;
				shake_screen(other.pow/2,image_angle);
			}
		}
		ds_list_clear(hit_list);
		
		last_x = x;
		last_y = y;
		last_angle = image_angle;
	}
	
} else if (carrying_log) {
	var adir = tdir + 15*dsin((legs_frame/12)*360)-60;

	axe.x = lerp(axe.x,rshoulder_x + xoff(8,2,adir),0.5)+hsp/2;
	axe.y = lerp(axe.y,rshoulder_y + yoff(8,2,adir),0.5)+vsp/2;

	axe.image_angle = rlerp(axe.image_angle,adir,0.2);
} else {
	
	lhand_offset = lerp(lhand_offset,12,0.05);
	lhand_dir = -1;
	var adir = tdir + 15*dsin((legs_frame/12)*360);

	axe.x = lerp(axe.x,rshoulder_x + xoff(12,0,adir),0.5)+hsp/2;
	axe.y = lerp(axe.y,rshoulder_y + yoff(12,0,adir),0.5)+vsp/2;

	axe.image_angle = rlerp(axe.image_angle,adir,0.2);
}


if (carrying_log) {
	rhand_offset = lerp(rhand_offset,6,0.05);
	logx = lerp(logx,x+xoff(0,-10,tdir),0.5)+hsp/2;
	logy = lerp(logy,y+yoff(0,-10,tdir),0.5)+vsp/2;
	logangle = rlerp(logangle,tdir,0.5);
} else {
	rhand_offset = lerp(rhand_offset,0,0.05);
}

var step = (floor(legs_frame) == 0) || (floor(legs_frame) == 6);
if (step && !last_step) {
	if(floor(legs_frame) == 0) {
		//left
		var sx = x+xoff(0,-4,ldir);
		var sy = y+yoff(0,-4,ldir);
	} else {
		var sx = x+xoff(0,+4,ldir);
		var sy = y+yoff(0,+4,ldir);
	}
	part_type_orientation(global.pt_footstep,ldir,ldir,0,0,0);
	part_particles_create(global.ps_floor,sx,sy,global.pt_footstep,1);
	if (position_meeting(sx,sy,oFloor)) {
		var fs = choose(aFootstepWood1,aFootstepWood2,aFootstepWood3,aFootstepWood4,aFootstepWood5,aFootstepWood6,aFootstepWood7,aFootstepWood8);
	} else {
		var fs = choose(aFootstepDirt1,aFootstepDirt2,aFootstepDirt3,aFootstepDirt4,aFootstepDirt5,aFootstepDirt6,aFootstepDirt7,aFootstepDirt8);
	}
	audio_play_sound_at(fs,sx,sy,16,32,64,1,false,10);
}
last_step = step;