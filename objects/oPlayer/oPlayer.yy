{
    "id": "c7fe982a-c1d0-465a-9c34-aa9eae1f9693",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "d52b11e5-19c6-40ad-8c0d-d5dbc3aa26ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c7fe982a-c1d0-465a-9c34-aa9eae1f9693"
        },
        {
            "id": "cbc2eb29-6750-4dcc-8a98-71754040970b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "c7fe982a-c1d0-465a-9c34-aa9eae1f9693"
        },
        {
            "id": "34326f8b-f07d-41fa-b8fd-6f2f835bb86d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c7fe982a-c1d0-465a-9c34-aa9eae1f9693"
        },
        {
            "id": "48537407-cb7b-4b0d-bb51-eee454d6feab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c7fe982a-c1d0-465a-9c34-aa9eae1f9693"
        },
        {
            "id": "0a3827c7-17e3-48ff-b953-35ea7e4673c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "c7fe982a-c1d0-465a-9c34-aa9eae1f9693"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6165bd54-f83a-453f-aa91-826784015ed5",
    "visible": true
}