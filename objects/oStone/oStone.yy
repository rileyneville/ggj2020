{
    "id": "1ee02f7f-08bc-45a3-b4b3-fe976c39ff58",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oStone",
    "eventList": [
        {
            "id": "b7e10093-585c-488d-9cdb-7ff41dd96c59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1ee02f7f-08bc-45a3-b4b3-fe976c39ff58"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2d6fc960-ffe4-4111-82b6-cf1ed6a801ec",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "37c7e25e-9879-4a1e-b8f7-d850c96978dd",
    "visible": true
}