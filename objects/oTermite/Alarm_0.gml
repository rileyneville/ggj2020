/// @description get target plank
target_plank = instance_nearest(x,y,oPlank);
if (!instance_exists(target_plank)) {	
	target_plank = instance_nearest(x,y,oWood);
}
other.alarm[0] = 180;