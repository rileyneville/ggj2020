/// @description
dir = random(360);
spd = 1;
acc = 0.1;
image_angle = dir;
hsp = 0;
vsp = 0;
target_plank = noone;
eat_speed = 0.025;
alarm[0] = 180;

food_required_for_egg = 6;
food = 0;