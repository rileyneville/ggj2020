/// @description
acc = 0.1;
if (instance_exists(target_plank)) {
	var tdir = dir;
	with (target_plank) {
		tdir = point_direction(other.x,other.y,x,y);
		
		if (place_meeting(x,y,other)) {
			damage_wood(id,other.x,other.y,other.dir,other.eat_speed);
			other.food += other.eat_speed;
			other.acc = 0.95;
		}
	}
	dir = rlerp(dir,tdir,random_range(0.025,0.2));
} else if (distance_to_object(oPlayer) < 64) {
	dir = rlerp(dir,point_direction(oPlayer.x,oPlayer.y,x,y),0.2);
} else {
	dir += random_range(-30,30);
}

if (food >= food_required_for_egg) {
	var egg = instance_create_depth(x,y,depth+10,oEgg);
	var edir = image_angle;
	egg.hsp = lengthdir_x(1.5,edir);
	egg.vsp = lengthdir_y(1.5,edir);
	food -= food_required_for_egg
}

hsp = lerp(hsp,lengthdir_x(spd,dir),acc);
vsp = lerp(vsp,lengthdir_y(spd,dir),acc);
image_angle = rlerp(image_angle,point_direction(0,0,hsp,vsp),0.5);

x += hsp;
y += vsp;
