{
    "id": "9dd7963c-2643-4f54-96d9-529f6fea4964",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTermite",
    "eventList": [
        {
            "id": "d33f03cc-f9cf-4ec2-b559-0101e288b275",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9dd7963c-2643-4f54-96d9-529f6fea4964"
        },
        {
            "id": "adbaf1a7-986b-4c6c-8efa-a017613e9ce6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9dd7963c-2643-4f54-96d9-529f6fea4964"
        },
        {
            "id": "2cb345a3-54a7-4767-9d4f-6eddd7f9e0a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9dd7963c-2643-4f54-96d9-529f6fea4964"
        },
        {
            "id": "572f99fa-000c-4b99-948c-542fd1f75cf0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "9dd7963c-2643-4f54-96d9-529f6fea4964"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "72bf9e60-cb24-417f-af86-5d2878b28997",
    "visible": true
}