{
    "id": "f827547f-39c8-4aea-8529-c5a88ce60cce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTermiteSpawner",
    "eventList": [
        {
            "id": "f0ff74f8-1a52-4c5f-99e4-34ab643a2aad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f827547f-39c8-4aea-8529-c5a88ce60cce"
        },
        {
            "id": "7b315c2c-1a92-4aa0-a0df-7f187dc01cdd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f827547f-39c8-4aea-8529-c5a88ce60cce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3ddf62f0-25b4-4070-80b9-cdbade86898c",
    "visible": false
}