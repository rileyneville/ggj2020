/// @description

// Inherit the parent event
event_inherited();

with (canopy) {
	falling = true;
	fall_dir = other.fall_dir;
}