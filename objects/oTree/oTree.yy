{
    "id": "1f5670f8-083d-4f3c-b35b-4b428a38ad6b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTree",
    "eventList": [
        {
            "id": "1292edf2-ad61-4516-a2e5-249d512b4594",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f5670f8-083d-4f3c-b35b-4b428a38ad6b"
        },
        {
            "id": "2a428c6d-35ce-455a-af13-8f6d18d6ec52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1f5670f8-083d-4f3c-b35b-4b428a38ad6b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a3fff901-09c7-4129-a881-b856b3460eb4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "87b5fc2b-0606-4f59-bab2-ff300f2a23e5",
    "visible": true
}