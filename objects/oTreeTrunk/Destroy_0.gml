/// @description

// Inherit the parent event
event_inherited();

for (var i = 0; i < 4; i++) {
	var offset = 10+i*18;
	var log = instance_create_depth(x+lengthdir_x(offset,image_angle),y+lengthdir_y(offset,image_angle),depth,oLog);
	log.image_angle = image_angle;
	log.image_yscale = random_sign();
	log.hsp = random_range(-1,1);
	log.vsp = random_range(-1,1);
}