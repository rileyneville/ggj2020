{
    "id": "0739ff53-3f66-4c6b-b45c-d387bca05f36",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTreeTrunk",
    "eventList": [
        {
            "id": "473097f8-b5fb-4064-9214-525f47280cda",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0739ff53-3f66-4c6b-b45c-d387bca05f36"
        },
        {
            "id": "0801750e-030e-4d0f-86de-426bf07a1aa7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "0739ff53-3f66-4c6b-b45c-d387bca05f36"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a3fff901-09c7-4129-a881-b856b3460eb4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "39f51e02-3f29-4ba3-8c4a-d3085713fa44",
    "visible": true
}