/// @description
part_type_direction(global.pt_splinter,0,360,0,0);
repeat(5) {
	var xx = random_range(bbox_left,bbox_right);
	var yy = random_range(bbox_top,bbox_bottom);
	part_particles_create(global.ps_floor,xx,yy,global.pt_splinter,1);
}

audio_play_sound_at(array_random(break_sounds),x,y,0,32,128,1,false,10);