{
    "id": "a3fff901-09c7-4129-a881-b856b3460eb4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWood",
    "eventList": [
        {
            "id": "b0fecade-3dca-41dc-a381-2fd4b1bf8ae5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a3fff901-09c7-4129-a881-b856b3460eb4"
        },
        {
            "id": "49074d64-530e-4a93-93bb-c6a62d53e6da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "a3fff901-09c7-4129-a881-b856b3460eb4"
        },
        {
            "id": "7f0b851b-5f39-4211-965f-0cf05700bb27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a3fff901-09c7-4129-a881-b856b3460eb4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2d6fc960-ffe4-4111-82b6-cf1ed6a801ec",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}