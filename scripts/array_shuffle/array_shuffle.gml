var list = array_to_list(argument0);
ds_list_shuffle(list);
var arr = list_to_array(list);
ds_list_destroy(list);
return arr;