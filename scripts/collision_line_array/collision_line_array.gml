
var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var object = argument4;
var array = is_array(object);

var collided = false;
if (!array) {
	collided = collision_line(x1,y1,x2,y2,object,true,false) != noone;
} else {
	for (var i = 0; i < array_length_1d(object);i++) {
		if (collision_line(x1,y1,x2,y2,object[i],true,false) != noone) {
			collided = true;
			break;
		}
	}
}
return collided;