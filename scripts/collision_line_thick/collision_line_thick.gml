///@param x1
///@param y1
///@param x2
///@param y2
///@param r
///@param object

var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var r = argument4;
var object = argument5;

var dir = point_direction(x1,y1,x2,y2);
var col = noone;
col = collision_line(x1,y1,x2,y2,object,true,false);
if (col == noone) {
	col = collision_line(x1+lengthdir_x(r,dir-90),y1+lengthdir_y(r,dir-90),
		x2+lengthdir_x(r,dir-90),y2+lengthdir_y(r,dir-90),object,true,false);
}
if (col == noone) {
	col = collision_line(x1+lengthdir_x(r,dir+90),y1+lengthdir_y(r,dir+90),
		x2+lengthdir_x(r,dir+90),y2+lengthdir_y(r,dir+90),object,true,false);
}
return col;
