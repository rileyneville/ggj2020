///@param wood_id
///@param x
///@param y
///@param dir
///@param damage_amount
var wid = argument0;
var xx = argument1;
var yy = argument2;
var dir = argument3;
var dmg = argument4;
with (wid) {
	fall_dir = dir + 180;
	if (((hp-dmg) mod splinter_frequency) > (hp mod splinter_frequency) || dmg >= splinter_frequency) {
		draw_angle = random_range(-5,5);
		part_type_direction(global.pt_splinter,dir+180-45,dir+180+45,0,0);
		part_particles_create(global.ps_floor,xx,yy,global.pt_splinter,min(ceil(dmg/splinter_frequency),20));
	} 
	
	
	if (hp <= dmg) {
		instance_destroy();
	} else {
		hp -= dmg;
	}
	image_index = (1-hp/10)*image_number;
	
}