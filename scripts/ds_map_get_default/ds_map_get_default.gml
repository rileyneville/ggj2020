///@param map
///@param key
///@param default
var val = ds_map_find_value(argument0,argument1);
if (is_undefined(val)) {
	val = argument2;
}
return val;