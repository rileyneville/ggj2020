///param gamepad index
for ( var i = gp_face1; i < gp_axisrv; i++ ) {
    if ( gamepad_button_check( argument0, i ) ) return true;
}
for ( var i = 0; i < gamepad_axis_count(argument0); i++ ) {
	if (gamepad_axis_value(argument0,i) != 0) {
		return true;
	}
}
return false;