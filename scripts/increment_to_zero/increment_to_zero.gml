///@param var
///@param [step]
var val = argument[0];
var step = 1;
if (argument_count > 1) {
	step = argument[1];
}
return val + sign(-val)*min(step,abs(val));