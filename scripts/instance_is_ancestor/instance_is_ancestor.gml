///@param instance
///@param ancestor_object_id
if (argument0 == argument1) {
	return true;
}
with (argument0) {
	return object_index == argument1 || object_is_ancestor(object_index,argument1);
}
return false;