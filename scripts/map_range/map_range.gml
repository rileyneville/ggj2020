//map_range(input,input_start,input_end,output_start,output_end)
///@param input
///@param input_start
///@param input_end
///@param output_start
///@param output_end
if (argument0-argument1 == 0) {
	return 	argument3;
}
return argument3 + ((argument4 - argument3) / (argument2 - argument1)) * (argument0 - argument1)