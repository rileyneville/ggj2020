///@param x1
///@param y1
///@param x2
///@param y2
///@param object
var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
if (point_distance(x1,y1,x2,y2) < 1) {
	return [x1,y1];
}
var object = argument4;
if (!collision_line_array(x1,y1,x2,y2,object)) {
	return [x2,y2];
}
while (point_distance(x1,y1,x2,y2) >= 1) {
	var midx = ((x1+x2)/2);
	var midy = ((y1+y2)/2);
	if (collision_line_array(x1,y1,midx,midy,object)) {
		x2 = midx;
		y2 = midy;
	} else {
		x1 = midx;
		y1 = midy;
	}
}
return [x1,y1];
