///@param x1
///@param y1
///@param length
///@param dir
///@param object
var x1 = argument0;
var y1 = argument1;
var ddist = argument2;
var ddir = argument3;
var object = argument4;
var x2 = x1+lengthdir_x(ddist,ddir);
var y2 = y1+lengthdir_y(ddist,ddir);

return raycast(x1,y1,x2,y2,object);
