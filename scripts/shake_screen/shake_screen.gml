///@param [shake_v]
///@param [shake_dir]
if (argument_count > 0) {
	var shake_amount = argument[0];
} else {
	shake_amount = 3;
}
shake_amount*= global.screenshake;
with (oCamera) {
	shake_v = max(shake_amount,abs(shake_v))*sign2(shake_v);
	if (argument_count > 1) {
		shake_dir = argument[1];
	} else {
		shake_dir = random(360);
	}
}