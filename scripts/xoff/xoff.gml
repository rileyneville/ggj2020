/// @description given an x and y offset, returns the x offset based on image angle.
///@param xoff
///@param yoff 
///@param [angle]
var xo = argument[0];
var yo = argument[1];
if (argument_count > 2) {
	var a = argument[2];
} else {
	a = image_angle;
}
return lengthdir_x(xo,a) + lengthdir_x(yo,a-90);