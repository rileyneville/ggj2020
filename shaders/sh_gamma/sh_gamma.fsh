//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float gamma;

void main()
{
	
    vec4 base_col = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );	
	
	base_col.rgb = pow(base_col.rgb,vec3(1./gamma));
	base_col.a = 1.;
	
	gl_FragColor = base_col;
}

