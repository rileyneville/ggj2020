{
    "id": "471b4585-2966-467b-b28e-eb5c532f5ff8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAxe",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89b1fc18-5c81-4918-a5a2-5323f8dda2e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "471b4585-2966-467b-b28e-eb5c532f5ff8",
            "compositeImage": {
                "id": "581d96e0-396b-43e0-89b7-dd0032d413ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89b1fc18-5c81-4918-a5a2-5323f8dda2e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6012a8d3-f908-46d3-bede-bc8a39f38e13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89b1fc18-5c81-4918-a5a2-5323f8dda2e4",
                    "LayerId": "c13abef5-d5ce-4faa-91e2-7e35190c88c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c13abef5-d5ce-4faa-91e2-7e35190c88c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "471b4585-2966-467b-b28e-eb5c532f5ff8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 22
}