{
    "id": "e9297f34-312c-4a8f-b97f-427c101415c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAxeSwing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3270420c-fa46-47ce-8911-ccf76ac841b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9297f34-312c-4a8f-b97f-427c101415c9",
            "compositeImage": {
                "id": "cb407621-bcf1-40b9-963d-f09bbe4fdd6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3270420c-fa46-47ce-8911-ccf76ac841b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b05da85-be18-4f6c-bc7d-7ab68e7acf9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3270420c-fa46-47ce-8911-ccf76ac841b8",
                    "LayerId": "3e03dad6-1004-491d-952f-9edd1e8264b9"
                }
            ]
        },
        {
            "id": "85fdb88c-636d-4e25-b4d9-a1fdf99fb3e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9297f34-312c-4a8f-b97f-427c101415c9",
            "compositeImage": {
                "id": "c14a9908-d986-498d-a245-ea5da1b765c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85fdb88c-636d-4e25-b4d9-a1fdf99fb3e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0af35f57-ffa2-46df-911b-5991564fab17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85fdb88c-636d-4e25-b4d9-a1fdf99fb3e3",
                    "LayerId": "3e03dad6-1004-491d-952f-9edd1e8264b9"
                }
            ]
        },
        {
            "id": "6c9bb984-3f77-44ea-8e1f-a2600a73b7d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9297f34-312c-4a8f-b97f-427c101415c9",
            "compositeImage": {
                "id": "8ce61242-e74a-4d1d-a9e7-1fe4ee80e107",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c9bb984-3f77-44ea-8e1f-a2600a73b7d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cfe044d-0a6e-4ac9-823c-11632b8644b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c9bb984-3f77-44ea-8e1f-a2600a73b7d8",
                    "LayerId": "3e03dad6-1004-491d-952f-9edd1e8264b9"
                }
            ]
        },
        {
            "id": "87ee9639-8785-4616-bbe2-d2708cecf815",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9297f34-312c-4a8f-b97f-427c101415c9",
            "compositeImage": {
                "id": "43f7f27c-69db-4f00-aafa-d2bd2b93fe09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87ee9639-8785-4616-bbe2-d2708cecf815",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a889b1a-bf37-4711-b88e-e0bd7bd22f06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87ee9639-8785-4616-bbe2-d2708cecf815",
                    "LayerId": "3e03dad6-1004-491d-952f-9edd1e8264b9"
                }
            ]
        },
        {
            "id": "54258052-8e33-4e9c-a5a5-b3c47e8ff9f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9297f34-312c-4a8f-b97f-427c101415c9",
            "compositeImage": {
                "id": "aa68d33a-0c42-4ff4-b4cf-520821398b3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54258052-8e33-4e9c-a5a5-b3c47e8ff9f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8979bd6f-4dba-43b7-b122-902dec43dc40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54258052-8e33-4e9c-a5a5-b3c47e8ff9f9",
                    "LayerId": "3e03dad6-1004-491d-952f-9edd1e8264b9"
                }
            ]
        },
        {
            "id": "5c0b4382-5f28-45a0-8acd-a5147aa3e621",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9297f34-312c-4a8f-b97f-427c101415c9",
            "compositeImage": {
                "id": "0567432d-4cca-4719-ba0e-8cfc935316d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c0b4382-5f28-45a0-8acd-a5147aa3e621",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ff03de8-1c56-496d-bee9-b3f026a33050",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c0b4382-5f28-45a0-8acd-a5147aa3e621",
                    "LayerId": "3e03dad6-1004-491d-952f-9edd1e8264b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3e03dad6-1004-491d-952f-9edd1e8264b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9297f34-312c-4a8f-b97f-427c101415c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}