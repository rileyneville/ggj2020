{
    "id": "e9097b78-36e4-4c53-8c0a-1748d35f06f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCanopy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b08a853e-0941-43c3-ab03-36c45dfad641",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9097b78-36e4-4c53-8c0a-1748d35f06f9",
            "compositeImage": {
                "id": "186b764c-634a-4e0f-ad65-edf43f125824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b08a853e-0941-43c3-ab03-36c45dfad641",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ca6266a-9dab-4a75-8a1f-ef12b3f32d84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b08a853e-0941-43c3-ab03-36c45dfad641",
                    "LayerId": "adc40547-5a15-48da-8145-5cabde614562"
                },
                {
                    "id": "20fe6aed-d401-463b-9e4d-f226bd7df187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b08a853e-0941-43c3-ab03-36c45dfad641",
                    "LayerId": "c744509f-0b51-4243-ad4d-755154153417"
                },
                {
                    "id": "cc62a8d2-8604-4b00-9803-59caa0182118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b08a853e-0941-43c3-ab03-36c45dfad641",
                    "LayerId": "fdf86cda-7941-4657-be4f-89d753abb89d"
                },
                {
                    "id": "48435869-7d75-4763-9886-bffa77952f82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b08a853e-0941-43c3-ab03-36c45dfad641",
                    "LayerId": "20b51537-d8b6-470c-a75f-c4edb7614ca3"
                }
            ]
        },
        {
            "id": "98988ed6-8b3c-4f3e-9c97-45793f447c8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9097b78-36e4-4c53-8c0a-1748d35f06f9",
            "compositeImage": {
                "id": "4dc25bbd-6a22-49f8-9dd4-1d179d50dfa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98988ed6-8b3c-4f3e-9c97-45793f447c8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdc14a93-9e60-4256-9ba4-ac9c045aa516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98988ed6-8b3c-4f3e-9c97-45793f447c8c",
                    "LayerId": "adc40547-5a15-48da-8145-5cabde614562"
                },
                {
                    "id": "a6087a3f-6406-41ba-a592-dfeb5b654d3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98988ed6-8b3c-4f3e-9c97-45793f447c8c",
                    "LayerId": "c744509f-0b51-4243-ad4d-755154153417"
                },
                {
                    "id": "b10f040d-e779-4b4a-a5f7-a46241e45fe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98988ed6-8b3c-4f3e-9c97-45793f447c8c",
                    "LayerId": "fdf86cda-7941-4657-be4f-89d753abb89d"
                },
                {
                    "id": "160939ea-b8a2-466f-8a94-5fef5710854d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98988ed6-8b3c-4f3e-9c97-45793f447c8c",
                    "LayerId": "20b51537-d8b6-470c-a75f-c4edb7614ca3"
                }
            ]
        },
        {
            "id": "e83b34eb-9e97-4b7d-99ac-1dd2123e743d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9097b78-36e4-4c53-8c0a-1748d35f06f9",
            "compositeImage": {
                "id": "f36fb4c9-ab2a-4135-b52a-98fef0b0624f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e83b34eb-9e97-4b7d-99ac-1dd2123e743d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d37763f5-dcca-4c58-941f-652ea1a0509c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e83b34eb-9e97-4b7d-99ac-1dd2123e743d",
                    "LayerId": "adc40547-5a15-48da-8145-5cabde614562"
                },
                {
                    "id": "2f76121c-437b-494d-afcb-1f21ad0ed8b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e83b34eb-9e97-4b7d-99ac-1dd2123e743d",
                    "LayerId": "c744509f-0b51-4243-ad4d-755154153417"
                },
                {
                    "id": "75ad7b4a-e7a0-475e-b233-25d4458c245e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e83b34eb-9e97-4b7d-99ac-1dd2123e743d",
                    "LayerId": "fdf86cda-7941-4657-be4f-89d753abb89d"
                },
                {
                    "id": "bd2f8887-8eb8-45fd-839e-1eea24bb498b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e83b34eb-9e97-4b7d-99ac-1dd2123e743d",
                    "LayerId": "20b51537-d8b6-470c-a75f-c4edb7614ca3"
                }
            ]
        },
        {
            "id": "60d8c9b6-5f96-43ce-8731-d7578157577e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9097b78-36e4-4c53-8c0a-1748d35f06f9",
            "compositeImage": {
                "id": "82e914c6-7dff-433f-a484-74f345a9740d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60d8c9b6-5f96-43ce-8731-d7578157577e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf0c9c66-5e87-4deb-8625-5795efe60677",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60d8c9b6-5f96-43ce-8731-d7578157577e",
                    "LayerId": "adc40547-5a15-48da-8145-5cabde614562"
                },
                {
                    "id": "ea906f8e-cf5c-415a-b7ae-93b2f887ccea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60d8c9b6-5f96-43ce-8731-d7578157577e",
                    "LayerId": "c744509f-0b51-4243-ad4d-755154153417"
                },
                {
                    "id": "1cf90383-c92c-4c35-93d6-b3fe3827c1bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60d8c9b6-5f96-43ce-8731-d7578157577e",
                    "LayerId": "fdf86cda-7941-4657-be4f-89d753abb89d"
                },
                {
                    "id": "82d56528-bd0e-4971-bb16-d31f144fb197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60d8c9b6-5f96-43ce-8731-d7578157577e",
                    "LayerId": "20b51537-d8b6-470c-a75f-c4edb7614ca3"
                }
            ]
        },
        {
            "id": "39db7ac3-83a3-4d20-909d-ce30956d48f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9097b78-36e4-4c53-8c0a-1748d35f06f9",
            "compositeImage": {
                "id": "d43a90a9-66b8-4da1-ac70-bc8b6c51e658",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39db7ac3-83a3-4d20-909d-ce30956d48f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f58f521-795d-47a6-a532-f9b54124e890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39db7ac3-83a3-4d20-909d-ce30956d48f5",
                    "LayerId": "fdf86cda-7941-4657-be4f-89d753abb89d"
                },
                {
                    "id": "6fb0aadf-f452-43db-8059-c5b1b4d96605",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39db7ac3-83a3-4d20-909d-ce30956d48f5",
                    "LayerId": "c744509f-0b51-4243-ad4d-755154153417"
                },
                {
                    "id": "8956400e-5ada-4244-a0de-32f0a437958e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39db7ac3-83a3-4d20-909d-ce30956d48f5",
                    "LayerId": "adc40547-5a15-48da-8145-5cabde614562"
                },
                {
                    "id": "d1f58027-b815-46d4-9912-6e6ec45cf567",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39db7ac3-83a3-4d20-909d-ce30956d48f5",
                    "LayerId": "20b51537-d8b6-470c-a75f-c4edb7614ca3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fdf86cda-7941-4657-be4f-89d753abb89d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9097b78-36e4-4c53-8c0a-1748d35f06f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c744509f-0b51-4243-ad4d-755154153417",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9097b78-36e4-4c53-8c0a-1748d35f06f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "adc40547-5a15-48da-8145-5cabde614562",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9097b78-36e4-4c53-8c0a-1748d35f06f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "20b51537-d8b6-470c-a75f-c4edb7614ca3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9097b78-36e4-4c53-8c0a-1748d35f06f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3 (2) (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}