{
    "id": "f7dc8530-20d9-43ee-b426-0cf8df54af40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCanopyParts",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e848278-b2c2-4215-9894-22252bccf5fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7dc8530-20d9-43ee-b426-0cf8df54af40",
            "compositeImage": {
                "id": "2069fe69-d434-45fa-bdac-58af54dde6a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e848278-b2c2-4215-9894-22252bccf5fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e1fcfd5-1bf6-46a6-9423-72b6982121c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e848278-b2c2-4215-9894-22252bccf5fc",
                    "LayerId": "6b5c9c03-4ca5-4d55-9d5f-c3d868e9f93e"
                }
            ]
        },
        {
            "id": "78aa0d73-74fe-401d-b386-859c92300c86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7dc8530-20d9-43ee-b426-0cf8df54af40",
            "compositeImage": {
                "id": "8b8f22b5-0ca9-4e82-8761-8ce800954d68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78aa0d73-74fe-401d-b386-859c92300c86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3555e0e-b5e5-4693-8850-33f0577f55d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78aa0d73-74fe-401d-b386-859c92300c86",
                    "LayerId": "6b5c9c03-4ca5-4d55-9d5f-c3d868e9f93e"
                }
            ]
        },
        {
            "id": "cf402deb-7be1-4f3d-8c8f-e138d577c392",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7dc8530-20d9-43ee-b426-0cf8df54af40",
            "compositeImage": {
                "id": "ac3c5c5a-9b5e-4d41-bb13-ecf62621bba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf402deb-7be1-4f3d-8c8f-e138d577c392",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ebaa04a-bce6-4711-aa58-cd35b4244e07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf402deb-7be1-4f3d-8c8f-e138d577c392",
                    "LayerId": "6b5c9c03-4ca5-4d55-9d5f-c3d868e9f93e"
                }
            ]
        },
        {
            "id": "483d066f-4bee-4bf9-8ad4-db16d5c89ddc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7dc8530-20d9-43ee-b426-0cf8df54af40",
            "compositeImage": {
                "id": "ff47efba-ff18-465d-8933-b75eaf15d5a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "483d066f-4bee-4bf9-8ad4-db16d5c89ddc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c60d759e-571d-439b-bd43-ab6475052fe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "483d066f-4bee-4bf9-8ad4-db16d5c89ddc",
                    "LayerId": "6b5c9c03-4ca5-4d55-9d5f-c3d868e9f93e"
                }
            ]
        },
        {
            "id": "ab6bfd16-9ea5-4a17-abf4-7c458aab3541",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7dc8530-20d9-43ee-b426-0cf8df54af40",
            "compositeImage": {
                "id": "96a92501-3ac3-4c28-b023-7db724943b97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab6bfd16-9ea5-4a17-abf4-7c458aab3541",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48364778-9224-466c-860d-3b30204ffa56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab6bfd16-9ea5-4a17-abf4-7c458aab3541",
                    "LayerId": "6b5c9c03-4ca5-4d55-9d5f-c3d868e9f93e"
                }
            ]
        },
        {
            "id": "d0557bf5-18a8-4efc-9085-ecd2796d3fc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7dc8530-20d9-43ee-b426-0cf8df54af40",
            "compositeImage": {
                "id": "96bab3ae-183c-44d5-995c-5a75f865886f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0557bf5-18a8-4efc-9085-ecd2796d3fc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26cfc71a-04bd-4e1b-a1d9-48e2c4ef266f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0557bf5-18a8-4efc-9085-ecd2796d3fc6",
                    "LayerId": "6b5c9c03-4ca5-4d55-9d5f-c3d868e9f93e"
                }
            ]
        },
        {
            "id": "aa3167f1-7654-4797-ae76-5795575781dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7dc8530-20d9-43ee-b426-0cf8df54af40",
            "compositeImage": {
                "id": "959884dd-477a-4c29-8464-d6aff7eaa054",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa3167f1-7654-4797-ae76-5795575781dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a020344-8d22-4871-b0b0-e4bcfc76a56c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa3167f1-7654-4797-ae76-5795575781dd",
                    "LayerId": "6b5c9c03-4ca5-4d55-9d5f-c3d868e9f93e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6b5c9c03-4ca5-4d55-9d5f-c3d868e9f93e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7dc8530-20d9-43ee-b426-0cf8df54af40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}