{
    "id": "cdbae85c-7263-42c6-8b01-31a39b8d7fa0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCircle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95274f82-d449-4a9e-9663-3336544df865",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cdbae85c-7263-42c6-8b01-31a39b8d7fa0",
            "compositeImage": {
                "id": "f9f9b817-3119-4d1e-bee1-3b7422d65e87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95274f82-d449-4a9e-9663-3336544df865",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa983d6d-5e55-4645-801a-8f67e56a90f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95274f82-d449-4a9e-9663-3336544df865",
                    "LayerId": "ab676d96-0c14-4a90-a893-4f022b0934e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ab676d96-0c14-4a90-a893-4f022b0934e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cdbae85c-7263-42c6-8b01-31a39b8d7fa0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}