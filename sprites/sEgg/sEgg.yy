{
    "id": "1b0f36a2-f0f8-4276-bf6b-f4a0145d3658",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEgg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 13,
    "bbox_right": 18,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "928177a8-1cd6-408d-9d06-06fa6575a876",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b0f36a2-f0f8-4276-bf6b-f4a0145d3658",
            "compositeImage": {
                "id": "05538cd2-15f2-4c56-8f9e-72fe45db27f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "928177a8-1cd6-408d-9d06-06fa6575a876",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff75d912-3f6b-42fd-943f-8ccc246878e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "928177a8-1cd6-408d-9d06-06fa6575a876",
                    "LayerId": "8be41a01-6fd3-4980-9363-2e6b919f58f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8be41a01-6fd3-4980-9363-2e6b919f58f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b0f36a2-f0f8-4276-bf6b-f4a0145d3658",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}