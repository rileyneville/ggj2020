{
    "id": "64b3d0ed-62a6-447e-88be-ffb97ab1ef17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEggShell",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 2,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d58bad7-661e-4ad0-9b2c-b4be11929a80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64b3d0ed-62a6-447e-88be-ffb97ab1ef17",
            "compositeImage": {
                "id": "617249a5-90dc-4d0a-9596-0241cf4021de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d58bad7-661e-4ad0-9b2c-b4be11929a80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66bd35b8-108f-41dd-bf4e-72f83c2e2626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d58bad7-661e-4ad0-9b2c-b4be11929a80",
                    "LayerId": "a788bec5-dac2-40d5-9f95-5ac228006c31"
                }
            ]
        },
        {
            "id": "02f45b45-c4e1-4952-b80d-cd0da3c27a1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64b3d0ed-62a6-447e-88be-ffb97ab1ef17",
            "compositeImage": {
                "id": "ab7e228a-1977-4321-b534-62342330e805",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02f45b45-c4e1-4952-b80d-cd0da3c27a1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71dda72b-75c4-43cb-80e2-b5234736184a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02f45b45-c4e1-4952-b80d-cd0da3c27a1f",
                    "LayerId": "a788bec5-dac2-40d5-9f95-5ac228006c31"
                }
            ]
        },
        {
            "id": "643f9f44-bb86-4ad2-8f35-6d887bce2bca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64b3d0ed-62a6-447e-88be-ffb97ab1ef17",
            "compositeImage": {
                "id": "876ced3b-d6bc-4b5d-9761-edebc3db52fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "643f9f44-bb86-4ad2-8f35-6d887bce2bca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d76b9911-5b86-4f33-adfb-f982f0b7fe8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "643f9f44-bb86-4ad2-8f35-6d887bce2bca",
                    "LayerId": "a788bec5-dac2-40d5-9f95-5ac228006c31"
                }
            ]
        },
        {
            "id": "f9033d5d-0074-4be9-ac96-7b20198f5222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64b3d0ed-62a6-447e-88be-ffb97ab1ef17",
            "compositeImage": {
                "id": "dde49eb1-78b3-4c24-acc6-247582cfe9ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9033d5d-0074-4be9-ac96-7b20198f5222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b3bd91f-6f27-46fc-a7d2-adafe2a973ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9033d5d-0074-4be9-ac96-7b20198f5222",
                    "LayerId": "a788bec5-dac2-40d5-9f95-5ac228006c31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "a788bec5-dac2-40d5-9f95-5ac228006c31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64b3d0ed-62a6-447e-88be-ffb97ab1ef17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3,
    "xorig": 1,
    "yorig": 1
}