{
    "id": "2f6e3d7b-de90-4f77-b3ad-39250a37ea84",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFloor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28ea764a-9d63-4fc0-b96a-59c699abcb6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f6e3d7b-de90-4f77-b3ad-39250a37ea84",
            "compositeImage": {
                "id": "a55f6148-2ba3-4125-9a5d-7a608a54cece",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28ea764a-9d63-4fc0-b96a-59c699abcb6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3df76d36-c0e4-466b-a791-f7a707e1a9ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28ea764a-9d63-4fc0-b96a-59c699abcb6a",
                    "LayerId": "25358a09-2e30-4530-bf33-222f6ea75be0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "25358a09-2e30-4530-bf33-222f6ea75be0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f6e3d7b-de90-4f77-b3ad-39250a37ea84",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}