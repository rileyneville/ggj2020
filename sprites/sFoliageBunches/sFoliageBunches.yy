{
    "id": "3c377c38-6360-4c13-9d46-b4e71a9399ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFoliageBunches",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b44c291-b242-44d5-8703-789b4471176d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c377c38-6360-4c13-9d46-b4e71a9399ca",
            "compositeImage": {
                "id": "20b3d40b-4f78-4651-9738-ba32bad0acd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b44c291-b242-44d5-8703-789b4471176d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c669aa3-af44-436d-803a-924656fb5b2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b44c291-b242-44d5-8703-789b4471176d",
                    "LayerId": "fbd27e95-0b6b-48c8-90f4-11d75a32f941"
                }
            ]
        },
        {
            "id": "31f53d50-97aa-4407-93b4-7b15a7bbf034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c377c38-6360-4c13-9d46-b4e71a9399ca",
            "compositeImage": {
                "id": "97980c3f-d7ea-43e0-a5c9-5aa3911a5b1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31f53d50-97aa-4407-93b4-7b15a7bbf034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "775c6084-2983-4341-90f9-b011b8f49242",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31f53d50-97aa-4407-93b4-7b15a7bbf034",
                    "LayerId": "fbd27e95-0b6b-48c8-90f4-11d75a32f941"
                }
            ]
        },
        {
            "id": "dd7b2561-49b2-4b93-908b-40bdb9776beb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c377c38-6360-4c13-9d46-b4e71a9399ca",
            "compositeImage": {
                "id": "bf375668-af6c-4464-bc9b-a88fdabfd76b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd7b2561-49b2-4b93-908b-40bdb9776beb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c649ed1-c978-4207-a967-b977399fd18b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd7b2561-49b2-4b93-908b-40bdb9776beb",
                    "LayerId": "fbd27e95-0b6b-48c8-90f4-11d75a32f941"
                }
            ]
        },
        {
            "id": "7fd54c4c-3f10-40cb-98ed-66479c5107e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c377c38-6360-4c13-9d46-b4e71a9399ca",
            "compositeImage": {
                "id": "e864d7d7-5c59-46a1-b515-4ecb1648095b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fd54c4c-3f10-40cb-98ed-66479c5107e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40c56dfd-dce4-47a3-bbee-4bfdc0bb8ed2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fd54c4c-3f10-40cb-98ed-66479c5107e6",
                    "LayerId": "fbd27e95-0b6b-48c8-90f4-11d75a32f941"
                }
            ]
        },
        {
            "id": "7449b9f0-7b53-469c-8f0e-a7d9c4b24e42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c377c38-6360-4c13-9d46-b4e71a9399ca",
            "compositeImage": {
                "id": "3a76ba27-c279-49fd-a5ef-f966c1cf4247",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7449b9f0-7b53-469c-8f0e-a7d9c4b24e42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f44522a3-fba0-4b7d-b106-17a06d5dddf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7449b9f0-7b53-469c-8f0e-a7d9c4b24e42",
                    "LayerId": "fbd27e95-0b6b-48c8-90f4-11d75a32f941"
                }
            ]
        },
        {
            "id": "79ab1f2f-10ba-4b9e-87b9-314804d89baf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c377c38-6360-4c13-9d46-b4e71a9399ca",
            "compositeImage": {
                "id": "dd260c6e-f82a-4236-8904-a013f546dd98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79ab1f2f-10ba-4b9e-87b9-314804d89baf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcfc55ee-69fd-40b3-a765-4d8feb035282",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79ab1f2f-10ba-4b9e-87b9-314804d89baf",
                    "LayerId": "fbd27e95-0b6b-48c8-90f4-11d75a32f941"
                }
            ]
        },
        {
            "id": "9938e531-2874-43e7-80c4-1bc09ad85514",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c377c38-6360-4c13-9d46-b4e71a9399ca",
            "compositeImage": {
                "id": "531716ba-63a5-424d-941d-a9a8aa9b7680",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9938e531-2874-43e7-80c4-1bc09ad85514",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41822759-ffa2-461d-bc6f-d4b40a0504a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9938e531-2874-43e7-80c4-1bc09ad85514",
                    "LayerId": "fbd27e95-0b6b-48c8-90f4-11d75a32f941"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fbd27e95-0b6b-48c8-90f4-11d75a32f941",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c377c38-6360-4c13-9d46-b4e71a9399ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}