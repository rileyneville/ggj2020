{
    "id": "07c1d5a0-5135-4f91-9f63-caa5e0703b65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFootstep",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 15,
    "bbox_right": 17,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3386d98f-16ca-4454-a0c7-1fdd41e59574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07c1d5a0-5135-4f91-9f63-caa5e0703b65",
            "compositeImage": {
                "id": "a005f38d-633d-4576-9e8a-14c11828fbf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3386d98f-16ca-4454-a0c7-1fdd41e59574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11c933ad-c550-40ea-8831-7d25b6a8d211",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3386d98f-16ca-4454-a0c7-1fdd41e59574",
                    "LayerId": "a2532927-5382-42e3-991c-326b2919e160"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a2532927-5382-42e3-991c-326b2919e160",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07c1d5a0-5135-4f91-9f63-caa5e0703b65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 25,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}