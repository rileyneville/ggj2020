{
    "id": "fb3d136a-0e9c-4af0-bdf3-dfb75bd195fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sInteractable",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 7,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "430d43bf-3113-464e-baa2-b8c789debb5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb3d136a-0e9c-4af0-bdf3-dfb75bd195fa",
            "compositeImage": {
                "id": "2fbfdfdf-4f57-48d6-8083-3a58b0bc1788",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "430d43bf-3113-464e-baa2-b8c789debb5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08f1e246-b41b-4b31-9793-52cf49898cf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "430d43bf-3113-464e-baa2-b8c789debb5a",
                    "LayerId": "208c4ce8-4e6e-4640-8e15-f442645fe601"
                }
            ]
        },
        {
            "id": "cd7ea371-f7e8-43f3-ab82-636f80fc3932",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb3d136a-0e9c-4af0-bdf3-dfb75bd195fa",
            "compositeImage": {
                "id": "0a8ec42c-3c91-4eb2-8a66-73d576817c8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd7ea371-f7e8-43f3-ab82-636f80fc3932",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "752a5c79-1cb0-4668-80e9-6c0b3898065b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd7ea371-f7e8-43f3-ab82-636f80fc3932",
                    "LayerId": "208c4ce8-4e6e-4640-8e15-f442645fe601"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "208c4ce8-4e6e-4640-8e15-f442645fe601",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb3d136a-0e9c-4af0-bdf3-dfb75bd195fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}