{
    "id": "e14bc9f5-5b96-44fd-a5e5-7e968dec95f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61b9cace-57f9-417f-9d8e-fda5bc679032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e14bc9f5-5b96-44fd-a5e5-7e968dec95f4",
            "compositeImage": {
                "id": "b17a24d8-bab6-4203-9ed9-519c7cae0eb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61b9cace-57f9-417f-9d8e-fda5bc679032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "591b61cd-20fb-460d-afa7-e1004c21aa50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61b9cace-57f9-417f-9d8e-fda5bc679032",
                    "LayerId": "c76de116-6a2f-419a-9ef9-50e31e0764a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "c76de116-6a2f-419a-9ef9-50e31e0764a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e14bc9f5-5b96-44fd-a5e5-7e968dec95f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 6
}