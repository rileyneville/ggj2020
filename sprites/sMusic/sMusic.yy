{
    "id": "f271a4a3-8c79-440d-b337-6ee091964192",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMusic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 10,
    "bbox_right": 21,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9fd0289-a5be-41ba-8a5c-270c1bc7ad87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f271a4a3-8c79-440d-b337-6ee091964192",
            "compositeImage": {
                "id": "73eee0ee-695f-410e-93d2-dba8040ca99f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9fd0289-a5be-41ba-8a5c-270c1bc7ad87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69793c93-e485-4606-a364-f8ae904de057",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9fd0289-a5be-41ba-8a5c-270c1bc7ad87",
                    "LayerId": "4e0e760b-e856-42a7-a7ea-775b4288c6da"
                }
            ]
        },
        {
            "id": "ed14b953-1d2d-4dbb-814e-cb77f03ce87d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f271a4a3-8c79-440d-b337-6ee091964192",
            "compositeImage": {
                "id": "a612db84-fcd2-420a-aee6-c066b4cffc78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed14b953-1d2d-4dbb-814e-cb77f03ce87d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c16da9a8-fff9-4758-8455-0b58f0c5a53e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed14b953-1d2d-4dbb-814e-cb77f03ce87d",
                    "LayerId": "4e0e760b-e856-42a7-a7ea-775b4288c6da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4e0e760b-e856-42a7-a7ea-775b4288c6da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f271a4a3-8c79-440d-b337-6ee091964192",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}