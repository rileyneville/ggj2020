{
    "id": "c71daa91-e09e-4420-a25f-ceaa98f431d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPixel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2fc964d0-21b7-49d2-bb03-171cd36c8217",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c71daa91-e09e-4420-a25f-ceaa98f431d4",
            "compositeImage": {
                "id": "8737b47f-f135-4561-9bd0-60fd1313097d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fc964d0-21b7-49d2-bb03-171cd36c8217",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be296410-14f9-4d19-b9eb-74887b26f358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fc964d0-21b7-49d2-bb03-171cd36c8217",
                    "LayerId": "332b34c6-1bcd-4c8f-8f7d-bf6c7fe64c1e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "332b34c6-1bcd-4c8f-8f7d-bf6c7fe64c1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c71daa91-e09e-4420-a25f-ceaa98f431d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}