{
    "id": "7e5fde26-f546-4503-a6f1-17ac64e7e138",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlank",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d6552cd-6c93-43ce-a091-cdd17eeabeb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5fde26-f546-4503-a6f1-17ac64e7e138",
            "compositeImage": {
                "id": "bc4ef21e-4aa4-48db-a221-154cc30b082f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d6552cd-6c93-43ce-a091-cdd17eeabeb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c4f9d93-ffc1-4ced-9c45-8fbe83af1c62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d6552cd-6c93-43ce-a091-cdd17eeabeb2",
                    "LayerId": "7b6f8af1-927a-4b71-8151-3ecc66ce1cf1"
                }
            ]
        },
        {
            "id": "147dc0aa-0df8-4ec2-8ba3-a3acad55cb46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5fde26-f546-4503-a6f1-17ac64e7e138",
            "compositeImage": {
                "id": "52cfb829-7d01-42c3-a2a2-9872aed9b69e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "147dc0aa-0df8-4ec2-8ba3-a3acad55cb46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb9a799d-ebaf-4740-b24a-19275ce4d130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "147dc0aa-0df8-4ec2-8ba3-a3acad55cb46",
                    "LayerId": "7b6f8af1-927a-4b71-8151-3ecc66ce1cf1"
                }
            ]
        },
        {
            "id": "05784552-de9c-4b62-b277-0c15915f94c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5fde26-f546-4503-a6f1-17ac64e7e138",
            "compositeImage": {
                "id": "140915d7-b452-48be-bfd8-df1b9ca0ee88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05784552-de9c-4b62-b277-0c15915f94c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd4f760a-4681-4a45-a154-21260d03c639",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05784552-de9c-4b62-b277-0c15915f94c1",
                    "LayerId": "7b6f8af1-927a-4b71-8151-3ecc66ce1cf1"
                }
            ]
        },
        {
            "id": "27e88db3-4682-41bb-8754-89c81f8c192f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5fde26-f546-4503-a6f1-17ac64e7e138",
            "compositeImage": {
                "id": "00795d29-6ac8-4ae0-b743-8f65b0dad963",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27e88db3-4682-41bb-8754-89c81f8c192f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91367b9c-8992-41d9-ad4c-c9657902e5e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27e88db3-4682-41bb-8754-89c81f8c192f",
                    "LayerId": "7b6f8af1-927a-4b71-8151-3ecc66ce1cf1"
                }
            ]
        },
        {
            "id": "dec6a71e-9f5d-41cc-8941-5594e370a3bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5fde26-f546-4503-a6f1-17ac64e7e138",
            "compositeImage": {
                "id": "aa4afa5c-51ab-48e3-8073-8c4d2ebf98d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dec6a71e-9f5d-41cc-8941-5594e370a3bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8e22c76-7ca0-4d4a-9af9-5c74b2f384b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dec6a71e-9f5d-41cc-8941-5594e370a3bf",
                    "LayerId": "7b6f8af1-927a-4b71-8151-3ecc66ce1cf1"
                }
            ]
        },
        {
            "id": "b4deb008-b439-4320-ab87-3f851073b2ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5fde26-f546-4503-a6f1-17ac64e7e138",
            "compositeImage": {
                "id": "25d299e6-01a0-4586-a9a0-99ff25b3761b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4deb008-b439-4320-ab87-3f851073b2ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94507048-51be-4c0c-bb00-a1900ba0664d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4deb008-b439-4320-ab87-3f851073b2ed",
                    "LayerId": "7b6f8af1-927a-4b71-8151-3ecc66ce1cf1"
                }
            ]
        },
        {
            "id": "c9496b83-2137-4eeb-92bf-04ed98d8bee1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5fde26-f546-4503-a6f1-17ac64e7e138",
            "compositeImage": {
                "id": "a61c0f81-c196-4913-9e27-d2e2d31ea060",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9496b83-2137-4eeb-92bf-04ed98d8bee1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "937eb2f9-d535-4a66-8945-e5dd8e5135e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9496b83-2137-4eeb-92bf-04ed98d8bee1",
                    "LayerId": "7b6f8af1-927a-4b71-8151-3ecc66ce1cf1"
                }
            ]
        },
        {
            "id": "81265570-5ceb-47b2-9cb3-ce656d0f77da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5fde26-f546-4503-a6f1-17ac64e7e138",
            "compositeImage": {
                "id": "a19f5530-b51a-4df8-822f-d8b7b5cdd30e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81265570-5ceb-47b2-9cb3-ce656d0f77da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9901e9c-cf90-4770-96af-ea5d080c0f5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81265570-5ceb-47b2-9cb3-ce656d0f77da",
                    "LayerId": "7b6f8af1-927a-4b71-8151-3ecc66ce1cf1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "7b6f8af1-927a-4b71-8151-3ecc66ce1cf1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e5fde26-f546-4503-a6f1-17ac64e7e138",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 17,
    "yorig": 2
}