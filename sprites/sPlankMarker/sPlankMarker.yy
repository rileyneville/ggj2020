{
    "id": "bced557e-b742-4db1-b220-f3eacf422a1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlankMarker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "023c6f20-e67e-480c-ae1a-466dda3db781",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bced557e-b742-4db1-b220-f3eacf422a1b",
            "compositeImage": {
                "id": "17469cdf-ec43-4a01-9f79-13f6abb3a604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "023c6f20-e67e-480c-ae1a-466dda3db781",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc44907b-4688-4c4e-bf1b-c8fe5da4782d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "023c6f20-e67e-480c-ae1a-466dda3db781",
                    "LayerId": "21372a46-fa74-476e-bed4-9253b4e851a2"
                }
            ]
        },
        {
            "id": "e148c7af-e0b2-4891-b4ab-13af255f4420",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bced557e-b742-4db1-b220-f3eacf422a1b",
            "compositeImage": {
                "id": "c900b285-eb25-465d-b6a2-e7fd4d9b4312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e148c7af-e0b2-4891-b4ab-13af255f4420",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b5bc367-a24f-42d3-b3b7-b36a0dc5268f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e148c7af-e0b2-4891-b4ab-13af255f4420",
                    "LayerId": "21372a46-fa74-476e-bed4-9253b4e851a2"
                }
            ]
        },
        {
            "id": "5f87f461-2795-49ed-8794-8f6e9decb8d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bced557e-b742-4db1-b220-f3eacf422a1b",
            "compositeImage": {
                "id": "3511df38-6f28-42d9-b82c-6d85a85673df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f87f461-2795-49ed-8794-8f6e9decb8d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76d190b5-9733-4435-9638-504816212154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f87f461-2795-49ed-8794-8f6e9decb8d7",
                    "LayerId": "21372a46-fa74-476e-bed4-9253b4e851a2"
                }
            ]
        },
        {
            "id": "c41ec1f1-d1dd-4acb-9894-95321518b713",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bced557e-b742-4db1-b220-f3eacf422a1b",
            "compositeImage": {
                "id": "46312e4b-1ca4-4c9a-9436-30d56e1f858b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c41ec1f1-d1dd-4acb-9894-95321518b713",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48370b97-c202-4620-be13-b6bc9038395e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c41ec1f1-d1dd-4acb-9894-95321518b713",
                    "LayerId": "21372a46-fa74-476e-bed4-9253b4e851a2"
                }
            ]
        },
        {
            "id": "c7d1b118-a85a-409c-9bb8-b4d81431f6bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bced557e-b742-4db1-b220-f3eacf422a1b",
            "compositeImage": {
                "id": "f235c924-2fd5-42fa-91b3-ef64254ed674",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7d1b118-a85a-409c-9bb8-b4d81431f6bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67583b13-bf4e-47f8-9904-4c3bbc26ed9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7d1b118-a85a-409c-9bb8-b4d81431f6bb",
                    "LayerId": "21372a46-fa74-476e-bed4-9253b4e851a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "21372a46-fa74-476e-bed4-9253b4e851a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bced557e-b742-4db1-b220-f3eacf422a1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 2
}