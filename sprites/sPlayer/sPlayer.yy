{
    "id": "6165bd54-f83a-453f-aa91-826784015ed5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 8,
    "bbox_right": 24,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9bddca2-f57c-46b1-9815-867532d33994",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6165bd54-f83a-453f-aa91-826784015ed5",
            "compositeImage": {
                "id": "87bf5a61-4fe5-41a7-8d41-d9d6bf41ac71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9bddca2-f57c-46b1-9815-867532d33994",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6d850eb-6e21-4288-9555-71dee0555dba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9bddca2-f57c-46b1-9815-867532d33994",
                    "LayerId": "434b7d90-bce1-44d5-b898-1411dc0e3e1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "434b7d90-bce1-44d5-b898-1411dc0e3e1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6165bd54-f83a-453f-aa91-826784015ed5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}