{
    "id": "15d80098-3056-4619-a8bb-5a5652ce55ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerHand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 8,
    "bbox_right": 24,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ce1109f-1acb-4b5b-8647-dd147e0d7e01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15d80098-3056-4619-a8bb-5a5652ce55ea",
            "compositeImage": {
                "id": "6877ef43-b9bd-4735-8500-09655a4aaecd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ce1109f-1acb-4b5b-8647-dd147e0d7e01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f60d5b6-0b29-4f59-bd9e-ba06dee35fb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ce1109f-1acb-4b5b-8647-dd147e0d7e01",
                    "LayerId": "264386ee-3358-46ee-92c8-7191f523e82f"
                }
            ]
        },
        {
            "id": "b2536f88-ec2c-424a-8e54-c6cc6b38e048",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15d80098-3056-4619-a8bb-5a5652ce55ea",
            "compositeImage": {
                "id": "32ec3b22-0d43-4b31-99ee-0bf4f9ec58de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2536f88-ec2c-424a-8e54-c6cc6b38e048",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a28ba93-a956-4fb1-9bae-683b49e09e6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2536f88-ec2c-424a-8e54-c6cc6b38e048",
                    "LayerId": "264386ee-3358-46ee-92c8-7191f523e82f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "264386ee-3358-46ee-92c8-7191f523e82f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15d80098-3056-4619-a8bb-5a5652ce55ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}