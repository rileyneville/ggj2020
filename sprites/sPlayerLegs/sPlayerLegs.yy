{
    "id": "6cdb3098-1e1a-4b39-88f4-7c964d4dda79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerLegs",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 8,
    "bbox_right": 24,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3929c0c1-0106-4f1e-bdd9-11c8f56ec6b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cdb3098-1e1a-4b39-88f4-7c964d4dda79",
            "compositeImage": {
                "id": "1a67e5ba-84bd-4c7f-8241-f4f6ceb67d8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3929c0c1-0106-4f1e-bdd9-11c8f56ec6b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d69896ac-41ed-412f-a8ef-9f380bd3a186",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3929c0c1-0106-4f1e-bdd9-11c8f56ec6b2",
                    "LayerId": "c47de35c-3d1f-4d3d-9963-8e387bebe840"
                },
                {
                    "id": "e5f1645a-23f3-4861-b301-b0fba4ee39d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3929c0c1-0106-4f1e-bdd9-11c8f56ec6b2",
                    "LayerId": "2fba72a3-88dc-4a84-94bc-f51ddd254d42"
                }
            ]
        },
        {
            "id": "3754b403-b2f4-4b08-b996-8f3b5201fff9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cdb3098-1e1a-4b39-88f4-7c964d4dda79",
            "compositeImage": {
                "id": "71737609-6c39-45a9-941d-bcfb17d8cd03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3754b403-b2f4-4b08-b996-8f3b5201fff9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01a1cb32-104a-4799-b3cc-5309b4307ba1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3754b403-b2f4-4b08-b996-8f3b5201fff9",
                    "LayerId": "c47de35c-3d1f-4d3d-9963-8e387bebe840"
                },
                {
                    "id": "d7632c74-41ee-49c4-bed9-ff7d0573a03e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3754b403-b2f4-4b08-b996-8f3b5201fff9",
                    "LayerId": "2fba72a3-88dc-4a84-94bc-f51ddd254d42"
                }
            ]
        },
        {
            "id": "7b7eb6bb-bfa9-4ec5-bf96-d59ef3b50335",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cdb3098-1e1a-4b39-88f4-7c964d4dda79",
            "compositeImage": {
                "id": "a260e90e-0ebb-4286-94ce-84eb8347b91d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b7eb6bb-bfa9-4ec5-bf96-d59ef3b50335",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "299628e2-4671-4600-994b-9f080b338a07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b7eb6bb-bfa9-4ec5-bf96-d59ef3b50335",
                    "LayerId": "c47de35c-3d1f-4d3d-9963-8e387bebe840"
                },
                {
                    "id": "d82467ed-220a-4d14-b4d7-15e195c0fc0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b7eb6bb-bfa9-4ec5-bf96-d59ef3b50335",
                    "LayerId": "2fba72a3-88dc-4a84-94bc-f51ddd254d42"
                }
            ]
        },
        {
            "id": "30b01704-5fc0-4c4b-baa5-018644d747ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cdb3098-1e1a-4b39-88f4-7c964d4dda79",
            "compositeImage": {
                "id": "7b45e255-5c52-4eb8-87df-f1492eb4b606",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30b01704-5fc0-4c4b-baa5-018644d747ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2183509-403c-42d3-95dc-34f9b344fca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30b01704-5fc0-4c4b-baa5-018644d747ba",
                    "LayerId": "c47de35c-3d1f-4d3d-9963-8e387bebe840"
                },
                {
                    "id": "61ac5f9f-3dfb-4cda-a569-8aa7f9bcfc24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30b01704-5fc0-4c4b-baa5-018644d747ba",
                    "LayerId": "2fba72a3-88dc-4a84-94bc-f51ddd254d42"
                }
            ]
        },
        {
            "id": "e18eac6b-74ca-4300-9c12-d7a4c2bf8b72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cdb3098-1e1a-4b39-88f4-7c964d4dda79",
            "compositeImage": {
                "id": "e5965693-a0e9-4d96-8907-fd0bd8cdc3e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e18eac6b-74ca-4300-9c12-d7a4c2bf8b72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b49662e5-6864-444e-a3da-788170d7243b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e18eac6b-74ca-4300-9c12-d7a4c2bf8b72",
                    "LayerId": "c47de35c-3d1f-4d3d-9963-8e387bebe840"
                },
                {
                    "id": "22f39bbb-5e62-4c9e-b14c-4f2492915ec0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e18eac6b-74ca-4300-9c12-d7a4c2bf8b72",
                    "LayerId": "2fba72a3-88dc-4a84-94bc-f51ddd254d42"
                }
            ]
        },
        {
            "id": "2f18231d-3b80-4bb8-832e-53bb161c7870",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cdb3098-1e1a-4b39-88f4-7c964d4dda79",
            "compositeImage": {
                "id": "53c82afa-1864-49d0-9828-e3c3ad8e487a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f18231d-3b80-4bb8-832e-53bb161c7870",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68feaf61-5ad1-4242-b6eb-73a2f2db4951",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f18231d-3b80-4bb8-832e-53bb161c7870",
                    "LayerId": "c47de35c-3d1f-4d3d-9963-8e387bebe840"
                },
                {
                    "id": "8754d963-5ca5-4d75-845b-a1eaf6464262",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f18231d-3b80-4bb8-832e-53bb161c7870",
                    "LayerId": "2fba72a3-88dc-4a84-94bc-f51ddd254d42"
                }
            ]
        },
        {
            "id": "f66e1b10-903e-4e74-b04c-6affdd2bdec8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cdb3098-1e1a-4b39-88f4-7c964d4dda79",
            "compositeImage": {
                "id": "e9630636-d173-4011-b13d-2c2cfffb6d5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f66e1b10-903e-4e74-b04c-6affdd2bdec8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4205ae5-6f67-40f6-96d3-75c1a7d7131c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f66e1b10-903e-4e74-b04c-6affdd2bdec8",
                    "LayerId": "c47de35c-3d1f-4d3d-9963-8e387bebe840"
                },
                {
                    "id": "c78ef11a-4f62-4a92-8cdc-1b77c816c210",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f66e1b10-903e-4e74-b04c-6affdd2bdec8",
                    "LayerId": "2fba72a3-88dc-4a84-94bc-f51ddd254d42"
                }
            ]
        },
        {
            "id": "d9b3e414-c10e-4a41-83b6-a681755fbf05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cdb3098-1e1a-4b39-88f4-7c964d4dda79",
            "compositeImage": {
                "id": "c0317888-3eb3-4113-b207-d1786c3b89de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9b3e414-c10e-4a41-83b6-a681755fbf05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4a42df3-4b8b-442b-bfcb-85d22fb538cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9b3e414-c10e-4a41-83b6-a681755fbf05",
                    "LayerId": "c47de35c-3d1f-4d3d-9963-8e387bebe840"
                },
                {
                    "id": "2a93243d-600a-4c5c-ace4-ef3854c02c93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9b3e414-c10e-4a41-83b6-a681755fbf05",
                    "LayerId": "2fba72a3-88dc-4a84-94bc-f51ddd254d42"
                }
            ]
        },
        {
            "id": "f953a070-4484-4f46-9d02-3af788b5bbba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cdb3098-1e1a-4b39-88f4-7c964d4dda79",
            "compositeImage": {
                "id": "c3c31562-9454-4d35-b116-0b774e7e2230",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f953a070-4484-4f46-9d02-3af788b5bbba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d50e65fa-74bb-4763-84b1-04baf13d22aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f953a070-4484-4f46-9d02-3af788b5bbba",
                    "LayerId": "c47de35c-3d1f-4d3d-9963-8e387bebe840"
                },
                {
                    "id": "ee560ff1-765e-48b7-8d55-87cdb62adc23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f953a070-4484-4f46-9d02-3af788b5bbba",
                    "LayerId": "2fba72a3-88dc-4a84-94bc-f51ddd254d42"
                }
            ]
        },
        {
            "id": "2f70ad28-315d-4f24-b497-fb7b94b79fc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cdb3098-1e1a-4b39-88f4-7c964d4dda79",
            "compositeImage": {
                "id": "8a5e811d-3524-472d-a175-3ad452bb169b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f70ad28-315d-4f24-b497-fb7b94b79fc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26aec4c9-5746-4836-bf58-f26d1f1f3973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f70ad28-315d-4f24-b497-fb7b94b79fc1",
                    "LayerId": "c47de35c-3d1f-4d3d-9963-8e387bebe840"
                },
                {
                    "id": "540fb481-46e6-49e9-8741-8142c42b8a6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f70ad28-315d-4f24-b497-fb7b94b79fc1",
                    "LayerId": "2fba72a3-88dc-4a84-94bc-f51ddd254d42"
                }
            ]
        },
        {
            "id": "acd4f161-a9a3-459b-95a1-cf204d8b4888",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cdb3098-1e1a-4b39-88f4-7c964d4dda79",
            "compositeImage": {
                "id": "49e14727-408d-4651-97c4-683cb3a13720",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acd4f161-a9a3-459b-95a1-cf204d8b4888",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "173d5b40-0371-4a73-b8a4-caf908d10eb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acd4f161-a9a3-459b-95a1-cf204d8b4888",
                    "LayerId": "c47de35c-3d1f-4d3d-9963-8e387bebe840"
                },
                {
                    "id": "9efedea4-2973-438c-8294-edb594e48686",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acd4f161-a9a3-459b-95a1-cf204d8b4888",
                    "LayerId": "2fba72a3-88dc-4a84-94bc-f51ddd254d42"
                }
            ]
        },
        {
            "id": "7a1f796c-6760-4772-b785-c097779e5bda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cdb3098-1e1a-4b39-88f4-7c964d4dda79",
            "compositeImage": {
                "id": "05b41e5d-841a-4e97-9b12-7a238390851f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a1f796c-6760-4772-b785-c097779e5bda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cc42f4e-9d73-4a52-a890-8a28f26faa2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a1f796c-6760-4772-b785-c097779e5bda",
                    "LayerId": "c47de35c-3d1f-4d3d-9963-8e387bebe840"
                },
                {
                    "id": "93372cd8-5c6f-4b3a-8561-c5093f2a4b06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a1f796c-6760-4772-b785-c097779e5bda",
                    "LayerId": "2fba72a3-88dc-4a84-94bc-f51ddd254d42"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c47de35c-3d1f-4d3d-9963-8e387bebe840",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cdb3098-1e1a-4b39-88f4-7c964d4dda79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "2fba72a3-88dc-4a84-94bc-f51ddd254d42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cdb3098-1e1a-4b39-88f4-7c964d4dda79",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}