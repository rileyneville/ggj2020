{
    "id": "88b5c44a-ebdc-45d8-83a6-ddcccdd3e632",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSound",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 11,
    "bbox_right": 22,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53482e20-f4f2-44f8-b815-6dcd968add8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88b5c44a-ebdc-45d8-83a6-ddcccdd3e632",
            "compositeImage": {
                "id": "de400e43-ccda-4ee3-b40b-cde62ba23539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53482e20-f4f2-44f8-b815-6dcd968add8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5209d403-fdef-4e59-85cf-ec76cc224723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53482e20-f4f2-44f8-b815-6dcd968add8e",
                    "LayerId": "a4ebe5e4-b0bd-4af8-8daa-e106081b6952"
                }
            ]
        },
        {
            "id": "d63e34f5-949e-4fcf-ac98-36fecd94b1a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88b5c44a-ebdc-45d8-83a6-ddcccdd3e632",
            "compositeImage": {
                "id": "1183bcb2-e152-4ecc-a1ff-1cdd7d63f148",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d63e34f5-949e-4fcf-ac98-36fecd94b1a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8c4c9b5-b815-41b6-9d9e-881417419fb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d63e34f5-949e-4fcf-ac98-36fecd94b1a9",
                    "LayerId": "a4ebe5e4-b0bd-4af8-8daa-e106081b6952"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a4ebe5e4-b0bd-4af8-8daa-e106081b6952",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88b5c44a-ebdc-45d8-83a6-ddcccdd3e632",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}