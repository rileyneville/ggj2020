{
    "id": "04e5d7b6-2c58-4ab1-967a-abb93774cb6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSparkle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 13,
    "bbox_right": 19,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a102cfb5-27ff-4717-9d91-688432fde4d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04e5d7b6-2c58-4ab1-967a-abb93774cb6f",
            "compositeImage": {
                "id": "7ba5d668-c10e-4c7f-bf91-84e64058ebc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a102cfb5-27ff-4717-9d91-688432fde4d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f02d8f8-9416-4513-97b4-79f64c6af050",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a102cfb5-27ff-4717-9d91-688432fde4d5",
                    "LayerId": "2b141730-5b61-4822-b02c-b1151e67aff4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2b141730-5b61-4822-b02c-b1151e67aff4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04e5d7b6-2c58-4ab1-967a-abb93774cb6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}