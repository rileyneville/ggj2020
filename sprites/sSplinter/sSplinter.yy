{
    "id": "ee94ef23-4646-4ab2-9239-92649dd2d99b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSplinter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8f9758e-1111-452d-8015-7220f8f1466e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee94ef23-4646-4ab2-9239-92649dd2d99b",
            "compositeImage": {
                "id": "975b2c08-cedc-4ded-af1e-4e3a9975a59b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8f9758e-1111-452d-8015-7220f8f1466e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6b4948a-9d2f-4b11-a049-2632bd627aab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8f9758e-1111-452d-8015-7220f8f1466e",
                    "LayerId": "fd078173-4536-4d17-8efd-77702b765e87"
                }
            ]
        },
        {
            "id": "23c5243d-1e85-4332-b6ee-899f098a40ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee94ef23-4646-4ab2-9239-92649dd2d99b",
            "compositeImage": {
                "id": "4c6a51ec-8507-4cd0-90fe-dee5f6623be5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23c5243d-1e85-4332-b6ee-899f098a40ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d88cebd-e8c9-423f-b264-8ec80a28e922",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23c5243d-1e85-4332-b6ee-899f098a40ed",
                    "LayerId": "fd078173-4536-4d17-8efd-77702b765e87"
                }
            ]
        },
        {
            "id": "7c3144b5-66e3-4c25-93cd-3b0af9e0f22b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee94ef23-4646-4ab2-9239-92649dd2d99b",
            "compositeImage": {
                "id": "48bde447-44a8-4512-ba8c-466561b52d6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c3144b5-66e3-4c25-93cd-3b0af9e0f22b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6554a9b-7287-40af-9e9b-11a8914bb627",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c3144b5-66e3-4c25-93cd-3b0af9e0f22b",
                    "LayerId": "fd078173-4536-4d17-8efd-77702b765e87"
                }
            ]
        },
        {
            "id": "30532956-5e30-4166-a1ad-73b61572816d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee94ef23-4646-4ab2-9239-92649dd2d99b",
            "compositeImage": {
                "id": "a5a66c8b-1bbf-4ea9-b189-60190cd52a0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30532956-5e30-4166-a1ad-73b61572816d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8002e00d-c425-4417-b86e-8eb75b191492",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30532956-5e30-4166-a1ad-73b61572816d",
                    "LayerId": "fd078173-4536-4d17-8efd-77702b765e87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "fd078173-4536-4d17-8efd-77702b765e87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee94ef23-4646-4ab2-9239-92649dd2d99b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 3,
    "yorig": 0
}