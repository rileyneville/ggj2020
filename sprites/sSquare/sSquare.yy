{
    "id": "3ddf62f0-25b4-4070-80b9-cdbade86898c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSquare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cff8b66c-2dc1-4bfa-9e41-b3a4fa2b1e7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ddf62f0-25b4-4070-80b9-cdbade86898c",
            "compositeImage": {
                "id": "7312d250-3cd0-4b35-99a1-cf9402faf7aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cff8b66c-2dc1-4bfa-9e41-b3a4fa2b1e7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01852f7d-934c-446c-b3e1-6b3d2c679561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cff8b66c-2dc1-4bfa-9e41-b3a4fa2b1e7c",
                    "LayerId": "e1efd1fd-c317-46c1-bc60-52246567744d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e1efd1fd-c317-46c1-bc60-52246567744d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ddf62f0-25b4-4070-80b9-cdbade86898c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 25,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}