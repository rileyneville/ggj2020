{
    "id": "37c7e25e-9879-4a1e-b8f7-d850c96978dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5dc50537-f78e-4741-a02b-c1dd2c28bd19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37c7e25e-9879-4a1e-b8f7-d850c96978dd",
            "compositeImage": {
                "id": "29aac24c-8f6c-47d0-b5cb-634c09d077f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dc50537-f78e-4741-a02b-c1dd2c28bd19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8db5ed5e-f54c-478e-b761-e3997cbfbca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dc50537-f78e-4741-a02b-c1dd2c28bd19",
                    "LayerId": "29a8f632-1847-4108-b3b2-0016347dd435"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "29a8f632-1847-4108-b3b2-0016347dd435",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37c7e25e-9879-4a1e-b8f7-d850c96978dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}