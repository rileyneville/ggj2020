{
    "id": "72bf9e60-cb24-417f-af86-5d2878b28997",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTermite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 10,
    "bbox_right": 20,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35f69f8a-c33c-4ad8-9952-513d587949e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72bf9e60-cb24-417f-af86-5d2878b28997",
            "compositeImage": {
                "id": "573c28c3-22fa-45f4-bf4f-e231293ad94f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35f69f8a-c33c-4ad8-9952-513d587949e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bbf37bb-8457-4713-8d0b-395c7e3e6b9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35f69f8a-c33c-4ad8-9952-513d587949e6",
                    "LayerId": "b2bb0a40-f8d5-43fa-a419-84748ae2e002"
                }
            ]
        },
        {
            "id": "4a4719d3-bfe0-4d31-b0cf-feeed47f8f3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72bf9e60-cb24-417f-af86-5d2878b28997",
            "compositeImage": {
                "id": "ce365cea-90bb-4b35-90d2-9a93212deda7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a4719d3-bfe0-4d31-b0cf-feeed47f8f3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f53041ad-3791-4764-9886-be39ccd24d06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a4719d3-bfe0-4d31-b0cf-feeed47f8f3e",
                    "LayerId": "b2bb0a40-f8d5-43fa-a419-84748ae2e002"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b2bb0a40-f8d5-43fa-a419-84748ae2e002",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72bf9e60-cb24-417f-af86-5d2878b28997",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}