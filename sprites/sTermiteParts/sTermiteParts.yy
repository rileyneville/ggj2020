{
    "id": "8dd01705-a09e-4ffd-ad67-6ca08e56079d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTermiteParts",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "741f0cc3-e2ae-4db0-9a24-643d2f9a0d46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dd01705-a09e-4ffd-ad67-6ca08e56079d",
            "compositeImage": {
                "id": "8dee5948-240c-4872-a409-5ac0841ba8d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "741f0cc3-e2ae-4db0-9a24-643d2f9a0d46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ddbc4be-9d63-4444-8965-9cf5a0f9d974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "741f0cc3-e2ae-4db0-9a24-643d2f9a0d46",
                    "LayerId": "4ae0fe3a-49fc-433b-87d4-0c2a31e7069e"
                }
            ]
        },
        {
            "id": "2ddcf390-2b8c-4433-9362-5f9d54e7d265",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dd01705-a09e-4ffd-ad67-6ca08e56079d",
            "compositeImage": {
                "id": "c11de872-f12a-4d96-9bcc-69010d2a26ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ddcf390-2b8c-4433-9362-5f9d54e7d265",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0562e947-8dfb-481b-ba01-cff616db45a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ddcf390-2b8c-4433-9362-5f9d54e7d265",
                    "LayerId": "4ae0fe3a-49fc-433b-87d4-0c2a31e7069e"
                }
            ]
        },
        {
            "id": "6fb3d9bc-3d71-4efb-a13e-94e5e4f95b13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dd01705-a09e-4ffd-ad67-6ca08e56079d",
            "compositeImage": {
                "id": "874c0f8b-bfdc-4ab8-b901-a78645480413",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fb3d9bc-3d71-4efb-a13e-94e5e4f95b13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01fd05fa-c241-4d6f-bc26-67bbad103e71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fb3d9bc-3d71-4efb-a13e-94e5e4f95b13",
                    "LayerId": "4ae0fe3a-49fc-433b-87d4-0c2a31e7069e"
                }
            ]
        },
        {
            "id": "1c6d3467-d078-4463-8578-bd445a2a624f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dd01705-a09e-4ffd-ad67-6ca08e56079d",
            "compositeImage": {
                "id": "ec55796d-d5e7-45de-bb7c-281b91411a74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c6d3467-d078-4463-8578-bd445a2a624f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54871d62-89be-4251-8d23-4513d82418b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c6d3467-d078-4463-8578-bd445a2a624f",
                    "LayerId": "4ae0fe3a-49fc-433b-87d4-0c2a31e7069e"
                }
            ]
        },
        {
            "id": "9709839c-cf48-4bf4-a677-1945f261591d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dd01705-a09e-4ffd-ad67-6ca08e56079d",
            "compositeImage": {
                "id": "5f7e6395-fb49-4d58-a6a5-af535ebdd2d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9709839c-cf48-4bf4-a677-1945f261591d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fa24a50-1d1d-402b-9044-b9fc9d964943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9709839c-cf48-4bf4-a677-1945f261591d",
                    "LayerId": "4ae0fe3a-49fc-433b-87d4-0c2a31e7069e"
                }
            ]
        },
        {
            "id": "7453ae83-5237-4277-b690-d6ceaf89e65f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dd01705-a09e-4ffd-ad67-6ca08e56079d",
            "compositeImage": {
                "id": "61f74116-3b30-46cc-9cfa-1609232c41da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7453ae83-5237-4277-b690-d6ceaf89e65f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85aeb98f-c4a6-4226-a84d-707ce20b7b59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7453ae83-5237-4277-b690-d6ceaf89e65f",
                    "LayerId": "4ae0fe3a-49fc-433b-87d4-0c2a31e7069e"
                }
            ]
        },
        {
            "id": "51681243-8f00-4db1-8f84-9043f6081967",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dd01705-a09e-4ffd-ad67-6ca08e56079d",
            "compositeImage": {
                "id": "f75ba19d-13dc-421b-b156-d0f90248fb15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51681243-8f00-4db1-8f84-9043f6081967",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21695835-697a-4cf4-97a7-5d1915b480cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51681243-8f00-4db1-8f84-9043f6081967",
                    "LayerId": "4ae0fe3a-49fc-433b-87d4-0c2a31e7069e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "4ae0fe3a-49fc-433b-87d4-0c2a31e7069e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8dd01705-a09e-4ffd-ad67-6ca08e56079d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 1
}