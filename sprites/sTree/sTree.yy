{
    "id": "87b5fc2b-0606-4f59-bab2-ff300f2a23e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 23,
    "bbox_right": 40,
    "bbox_top": 23,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7479e11e-f941-47ba-a9c1-7be8a5af5534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87b5fc2b-0606-4f59-bab2-ff300f2a23e5",
            "compositeImage": {
                "id": "75a849ce-7bd6-4f48-819e-6baf5e2b71a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7479e11e-f941-47ba-a9c1-7be8a5af5534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d0d5c9b-d418-49e0-a7cb-56d7abec17d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7479e11e-f941-47ba-a9c1-7be8a5af5534",
                    "LayerId": "9c8781eb-7acc-4619-9321-74be58d64645"
                }
            ]
        },
        {
            "id": "506ef32f-df0e-48cd-99cd-4f426489209c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87b5fc2b-0606-4f59-bab2-ff300f2a23e5",
            "compositeImage": {
                "id": "8b7f6787-2338-4e0b-ad88-b8385d930a67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "506ef32f-df0e-48cd-99cd-4f426489209c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c1238d0-e165-4ce5-b189-e6cdb0b10ea9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "506ef32f-df0e-48cd-99cd-4f426489209c",
                    "LayerId": "9c8781eb-7acc-4619-9321-74be58d64645"
                }
            ]
        },
        {
            "id": "4c7ad5f3-ed60-4dbc-9b5a-a30059804dde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87b5fc2b-0606-4f59-bab2-ff300f2a23e5",
            "compositeImage": {
                "id": "78b07c45-1ad3-44ef-b6e3-a6f7d692fa5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c7ad5f3-ed60-4dbc-9b5a-a30059804dde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5413e226-35b2-4bee-91bb-cb8a1077e6df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c7ad5f3-ed60-4dbc-9b5a-a30059804dde",
                    "LayerId": "9c8781eb-7acc-4619-9321-74be58d64645"
                }
            ]
        },
        {
            "id": "6fefd448-129c-4179-bc32-2a21233837d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87b5fc2b-0606-4f59-bab2-ff300f2a23e5",
            "compositeImage": {
                "id": "752e44af-6116-4702-a033-a6a3d4e01ad7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fefd448-129c-4179-bc32-2a21233837d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24f03ee1-78d6-4a14-b676-5eca0bc66c9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fefd448-129c-4179-bc32-2a21233837d4",
                    "LayerId": "9c8781eb-7acc-4619-9321-74be58d64645"
                }
            ]
        },
        {
            "id": "60447569-48f3-4f24-a25d-75e8f385b94e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87b5fc2b-0606-4f59-bab2-ff300f2a23e5",
            "compositeImage": {
                "id": "9e3ee7ea-3337-410a-9d5d-4d5575791bb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60447569-48f3-4f24-a25d-75e8f385b94e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8cfa531-4b5e-4500-9d52-171f547ea047",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60447569-48f3-4f24-a25d-75e8f385b94e",
                    "LayerId": "9c8781eb-7acc-4619-9321-74be58d64645"
                }
            ]
        },
        {
            "id": "c4d37df1-8bf7-47fb-ad1b-ea42488d912c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87b5fc2b-0606-4f59-bab2-ff300f2a23e5",
            "compositeImage": {
                "id": "4b26ca40-ed01-402e-909f-f3adaa7cfce0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4d37df1-8bf7-47fb-ad1b-ea42488d912c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e90f27d-3ac8-4d7f-aeee-db41d49145df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4d37df1-8bf7-47fb-ad1b-ea42488d912c",
                    "LayerId": "9c8781eb-7acc-4619-9321-74be58d64645"
                }
            ]
        },
        {
            "id": "e7ae6908-dd7a-4b68-8de1-cfe6464c0005",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87b5fc2b-0606-4f59-bab2-ff300f2a23e5",
            "compositeImage": {
                "id": "5f5bc618-133f-467f-87e5-ce4670428265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7ae6908-dd7a-4b68-8de1-cfe6464c0005",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfeb5290-9d66-440c-88cd-8434d4de78fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7ae6908-dd7a-4b68-8de1-cfe6464c0005",
                    "LayerId": "9c8781eb-7acc-4619-9321-74be58d64645"
                }
            ]
        },
        {
            "id": "0588a861-89de-4b03-9be8-b21ad882b72e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87b5fc2b-0606-4f59-bab2-ff300f2a23e5",
            "compositeImage": {
                "id": "27af0dfa-3384-4d73-bcee-56df75b0478e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0588a861-89de-4b03-9be8-b21ad882b72e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21838fd1-4f6a-486b-8124-9b843f34ac8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0588a861-89de-4b03-9be8-b21ad882b72e",
                    "LayerId": "9c8781eb-7acc-4619-9321-74be58d64645"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9c8781eb-7acc-4619-9321-74be58d64645",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87b5fc2b-0606-4f59-bab2-ff300f2a23e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}