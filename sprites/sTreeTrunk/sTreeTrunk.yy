{
    "id": "39f51e02-3f29-4ba3-8c4a-d3085713fa44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTreeTrunk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 3,
    "bbox_right": 81,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b261711-1e06-4e50-8857-c23a69ebc508",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39f51e02-3f29-4ba3-8c4a-d3085713fa44",
            "compositeImage": {
                "id": "fb799beb-1a3b-4bed-b9e9-8255b42ef8ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b261711-1e06-4e50-8857-c23a69ebc508",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb7c12aa-9464-4b37-9ed4-cc7e5a6363c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b261711-1e06-4e50-8857-c23a69ebc508",
                    "LayerId": "3dd4fdd8-42bf-4f41-8a69-fa9f9799d132"
                }
            ]
        },
        {
            "id": "b33c2405-e00b-48e2-8aa9-4ed7b496417e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39f51e02-3f29-4ba3-8c4a-d3085713fa44",
            "compositeImage": {
                "id": "8682b12b-83b2-499c-9af3-b045ad6a666d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b33c2405-e00b-48e2-8aa9-4ed7b496417e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8dd2587-c41e-4866-b431-d0cb6c849930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b33c2405-e00b-48e2-8aa9-4ed7b496417e",
                    "LayerId": "3dd4fdd8-42bf-4f41-8a69-fa9f9799d132"
                }
            ]
        },
        {
            "id": "d7de239c-60cb-4929-bf1e-af7ba2f9c6ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39f51e02-3f29-4ba3-8c4a-d3085713fa44",
            "compositeImage": {
                "id": "855de215-24ae-4f91-8946-5faa9ae79faa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7de239c-60cb-4929-bf1e-af7ba2f9c6ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d64781e-11a9-45bd-b399-7fad9187232a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7de239c-60cb-4929-bf1e-af7ba2f9c6ee",
                    "LayerId": "3dd4fdd8-42bf-4f41-8a69-fa9f9799d132"
                }
            ]
        },
        {
            "id": "e1844917-bb91-4100-955e-35571f543ec9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39f51e02-3f29-4ba3-8c4a-d3085713fa44",
            "compositeImage": {
                "id": "26606426-a843-4465-a6f1-a30512b9c17e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1844917-bb91-4100-955e-35571f543ec9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4fd94f5-6112-4297-8656-13c83bf9ff31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1844917-bb91-4100-955e-35571f543ec9",
                    "LayerId": "3dd4fdd8-42bf-4f41-8a69-fa9f9799d132"
                }
            ]
        },
        {
            "id": "feba9130-e3ff-475d-af72-289e2583ab5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39f51e02-3f29-4ba3-8c4a-d3085713fa44",
            "compositeImage": {
                "id": "36011245-b28e-4306-ac7d-c01d431d72af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feba9130-e3ff-475d-af72-289e2583ab5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e30b938-8999-4d54-b027-dcc3bb0f61c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feba9130-e3ff-475d-af72-289e2583ab5f",
                    "LayerId": "3dd4fdd8-42bf-4f41-8a69-fa9f9799d132"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "3dd4fdd8-42bf-4f41-8a69-fa9f9799d132",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39f51e02-3f29-4ba3-8c4a-d3085713fa44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 24
}